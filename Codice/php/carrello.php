<?php
    session_start();
    $redirect = true;
    require_once 'util.php';
    if(utente_collegato()){
        if(isset($_GET['r']) && isset($_GET['p']) && isset($_GET['q']) && isset($_COOKIE['carrello'])){ //Rimuovi dal carrello
            $carrello = json_decode($_COOKIE['carrello'], true);
            if($_GET['p'] === 'all'){ //Rimuovi tutto
                setcookie('carrello', '', strtotime('-1 year'));
                unset($_COOKIE['carrello']);
            }elseif(isset($carrello[$_GET['p']])){ //Rimuovi una quantità di un prodotto

                if(is_numeric($carrello[$_GET['p']])){
                    $carrello[$_GET['p']] -= $_GET['q'];
                }else{
                    $carrello[$_GET['p']] = 0;
                }
                if($carrello[$_GET['p']] == 0){
                    unset($carrello[$_GET['p']]);
                }
                setcookie('carrello', json_encode($carrello), strtotime('+1 year'));
                
            }
        }elseif(isset($_GET['p']) && isset($_GET['q'])){ //Aggiungi al carrello
            $carrello = [];
            if(isset($_COOKIE['carrello'])){
                $carrello = json_decode($_COOKIE['carrello'], true);
            }
            if(isset($carrello[$_GET['p']]) && is_numeric($carrello[$_GET['p']])){
                $carrello[$_GET['p']] += $_GET['q'];
            }else{
                $carrello[$_GET['p']] = $_GET['q'];
            }
            setcookie('carrello', json_encode($carrello), strtotime('+1 year'));
        }else{ //Visualizza il contenuto del carrello
            $carrello = json_decode($_COOKIE['carrello'] ?? '[]', true); ?>
            <h1>Carrello</h1>
            <?php if(count($carrello) > 0){
                $error = false;
                require_once "connessioneDB.php";
                $in_clouse = "";
                $in_clouse_data = [];
                $in_clouse_param_types = "";

                foreach ($carrello as $key => $value) {
                    $in_clouse .= '?,';
                    $in_clouse_data[] = &$key;
                    $in_clouse_param_types .= 's';
                    unset($key);
                }
                $in_clouse = substr($in_clouse, 0, -strlen(","));

                $sql = "SELECT 
                            prodotto.nome, prodotto.codice_a_barre, prodotto.descrizione,
                            prodotto.iva, prodotto.prezzo_finale, reparto.nome as 'r_nome', 
                            fornitore.nome as 'f_nome', prodotto.peso
                        FROM prodotto
                        INNER JOIN reparto ON(prodotto.id_reparto = reparto.id)
                        INNER JOIN fornitore ON(prodotto.id_fornitore = fornitore.id)
                        WHERE prodotto.codice_a_barre IN($in_clouse)
                        ORDER BY prodotto.nome ASC";
                $stmt = null;
                if(
                    ($stmt = $conn->prepare($sql)) === false ||
                    $stmt->bind_param($in_clouse_param_types, ...$in_clouse_data) === false ||
                    $stmt->execute() === false
                ){
                    $error = true;
                }
                
                if(!$error){
                    $redirect = false;
                    $carr = $stmt->get_result();
                    if($carr->num_rows > 0): ?>
                        <table border="1">
                            <tr>
                                <th>Prodotto</th>
                                <th>Codice a Barre</th>
                                <th>Descrizione</th>
                                <th>Iva</th>
                                <th>Prezzo Finale</th>
                                <th>Reparto</th>
                                <th>Fornitore</th>
                                <th>Peso</th>
                                <th>Quantit&agrave;</th>
                                <th>Rimuovi</th>
                                <th>Aggiungi</th>
                                <th>Rimuovi Tutto</th>
                            </tr>
                            <?php
                                while($prod = $carr->fetch_assoc()): ?>

                                    <tr>
                                        <td><?php echo $prod['nome']; ?></td>
                                        <td><?php echo $prod['codice_a_barre']; ?></td>
                                        <td><?php echo $prod['descrizione']; ?></td>
                                        <td><?php echo $prod['iva']; ?></td>
                                        <td><?php echo $prod['prezzo_finale']; ?></td>
                                        <td><?php echo $prod['r_nome']; ?></td>
                                        <td><?php echo $prod['f_nome']; ?></td>
                                        <td><?php echo $prod['peso']; ?></td>
                                        <td><?php echo htmlspecialchars($carrello[$prod['codice_a_barre']]); ?></td>
                                        <td><a href="./carrello.php?r=1&p=<?php echo $prod['codice_a_barre']; ?>&q=1">-</a></td>
                                        <td><a href="./carrello.php?p=<?php echo $prod['codice_a_barre']; ?>&q=1">+</a></td>
                                        <td><a href="./carrello.php?r=1&p=<?php echo $prod['codice_a_barre']; ?>&q=<?php echo htmlspecialchars($carrello[$prod['codice_a_barre']]); ?>">Rimuovi dal carrello</a></td>
                                    </tr>

                                <?php endwhile;
                            ?>
                        </table>
                        <a href="./carrello.php?r=1&p=all&q=">Svuota carrello</a>
                        <a href="./ordina.php">Ordina tutto</a>
                    <?php else: ?>
                        <p>Non ci sono prodotti nel carrello. Aggiungine dalla <a href="./prodotti.php">Pagina dei prodotti</a></p>
                    <?php endif;
                }
                close_conn();
            }else{ ?>
                <p>Non ci sono prodotti nel carrello. Aggiungine dalla <a href="./prodotti.php">Pagina dei prodotti</a></p>
            <?php 
                $redirect = false;
            }
            ?>
                <a href="./index.php">Home</a>
            <?php
        }
    }
    if($redirect){
        $referrer = parse_url($_SERVER['HTTP_REFERER']);
        $referrer_path = $referrer['path'] ?? 'prodotti.php';
        $referrer_query = isset($referrer['path']) ? ($referrer['query'] ?? '') : '';
        $referrer = $referrer_path . '?' . $referrer_query;
        header("location: $referrer");
        die();
    }
?>
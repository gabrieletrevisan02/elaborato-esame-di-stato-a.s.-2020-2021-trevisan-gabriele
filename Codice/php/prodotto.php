<?php
    if(!isset($_GET['p'])){
        header('location: prodotti.php');
        die();
    }
    
    $codice_prodotto = $_GET['p'];

    $pg_ricerca = $_GET['pg_r'] ?? '';

    session_start();
    
    require_once "connessioneDB.php";
    require_once "util.php";
    /*
        Prodotti vendibili online: tutti quelli presenti nei magazzini centrali, con la flag "vendibile online"
        Prodotti disponibili solo in negozio: tutti i prodotti presenti nei magazzini dei negozzi
    */
        
    $prodotto = null;
    ($stmt = $conn->prepare(
        "SELECT
            prodotto.codice_a_barre, prodotto.nome AS 'p_nome', prodotto.descrizione,
            prodotto.prezzo_finale, reparto.nome AS 'r_nome', fornitore.nome AS 'f_nome',
            prodotto.vendibile_online
         FROM prodotto
         INNER JOIN reparto ON(prodotto.id_reparto = reparto.id)
         INNER JOIN fornitore ON(prodotto.id_fornitore = fornitore.id)
         WHERE prodotto.codice_a_barre = ?"
    )) === false ||
    $stmt->bind_param('s', $codice_prodotto) === false ||
    $stmt->execute() === false ||
    ($prodotto = $stmt->get_result()->fetch_assoc()) === false;
    
    $prod_reali = [
        'online' => [
            'posizioni' => [],
            'disp' => [],
            'sconti' => [],
        ],
        'negozzi' => [
            'posizioni' => [],
            'disp' => [],
            'sconti' => [],
        ],
    ];
    //Quantità disponibile per la vendita online
    if($prodotto['vendibile_online'] == 1){
        $online = null;
        ($stmt = $conn->prepare(
            "SELECT
                magazzino_centrale.posizione, prodotto_magazzino_centrale.quant as 'disp', prodotto_magazzino_centrale.sconto
            FROM prodotto
            LEFT JOIN prodotto_magazzino_centrale ON(prodotto.codice_a_barre = prodotto_magazzino_centrale.codice_prodotto)
            LEFT JOIN magazzino_centrale ON(prodotto_magazzino_centrale.id_magazzino_centrale = magazzino_centrale.id)
            INNER JOIN reparto ON(prodotto.id_reparto = reparto.id)
            INNER JOIN fornitore ON(prodotto.id_fornitore = fornitore.id)
            WHERE prodotto.codice_a_barre = ?"
        )) === false ||
        $stmt->bind_param('s', $codice_prodotto) === false ||
        $stmt->execute() === false ||
        ($online = $stmt->get_result()) === false;

        while($row = $online->fetch_assoc()){
            $prod_reali['online']['posizioni'][] = $row['posizione'];
            $prod_reali['online']['disp'][] = $row['disp'];
            $prod_reali['online']['sconti'][] = $row['sconto'];
        }
    }
    //Quantità disponibile nei negozi
    $in_negozio = null;
    ($stmt = $conn->prepare(
        "SELECT
           magazzino.posizione, prodotto_magazzino.quant as 'disp', prodotto_magazzino.sconto
         FROM prodotto
         LEFT JOIN prodotto_magazzino ON(prodotto.codice_a_barre = prodotto_magazzino.codice_prodotto)
         LEFT JOIN magazzino ON(prodotto_magazzino.id_magazzino = magazzino.id)
         INNER JOIN reparto ON(prodotto.id_reparto = reparto.id)
         INNER JOIN fornitore ON(prodotto.id_fornitore = fornitore.id)
         WHERE prodotto.codice_a_barre = ?"
    )) === false ||
    $stmt->bind_param('s', $codice_prodotto) === false ||
    $stmt->execute() === false ||
    ($in_negozio = $stmt->get_result()) === false;

    while($row = $in_negozio->fetch_assoc()){
        $prod_reali['negozzi']['posizioni'][] = $row['posizione'];
        $prod_reali['negozzi']['disp'][] = $row['disp'];
        $prod_reali['negozzi']['sconti'][] = $row['sconto'];
    }
    close_conn(); 
?>

<html>
    <head>
        <title>Prodotto</title>
    </head>
    <body>
        <h1>Dettagli prodotto</h1>
        <div>
            <div>
                <h1><?php echo htmlspecialchars($prodotto['p_nome']); ?></h1>
                <p><?php  echo htmlspecialchars($prodotto['codice_a_barre']); ?></p>
                <p>Prezzo: <?php  echo htmlspecialchars(number_format(round($prodotto['prezzo_finale'] / 100.0, 2), 2, ',', '')); ?>&euro;</p>
                <p>Descrizione: <?php  echo htmlspecialchars($prodotto['descrizione']); ?></p>
                <p>Reparto: <?php  echo htmlspecialchars($prodotto['r_nome']); ?></p>
                <p>Fornitore: <?php  echo htmlspecialchars($prodotto['f_nome']); ?></p>
                <h3>Disponibilit&agrave; Online</h3>
                <?php if($prodotto['vendibile_online'] == FALSE): ?>
                    <p style="color:red;font-size:medium;">Prodotto non disponibile online</p>
                <?php elseif(count($prod_reali['online']['disp']) > 0 && $prod_reali['online']['disp'][0] !== NULL): ?>
                        <table>
                            <tr><th>Posizione</th><th>Disponibilit&agrave;</th><th>Sconto</th></tr>
                            <?php for($i = 0; $i < count($prod_reali['online']['disp']); ++$i) : ?>
                                <tr> 
                                    <td><?php echo htmlspecialchars($prod_reali['online']['posizioni'][$i]);   ?></td>
                                    <td><?php echo htmlspecialchars($prod_reali['online']['disp'][$i]);        ?></td>
                                    <td><?php echo htmlspecialchars($prod_reali['online']['sconti'][$i] ?? 0); ?>%</td>
                                </tr>
                            <?php endfor; ?>
                        </table>
                <?php else: ?>
                    <p style="color:red;font-size:medium;">Prodotto attualmente non disponibile online</p>
                <?php endif; ?>
                <h3>Disponibilit&agrave; nei Negozzi</h3>
                <?php if(count($prod_reali['negozzi']['disp']) > 0 && $prod_reali['negozzi']['disp'][0] !== NULL): ?>
                    <table>
                        <tr><th>Posizione</th><th>Disponibilit&agrave;</th><th>Sconto</th></tr>
                        <?php for($i = 0; $i < count($prod_reali['negozzi']['disp']); ++$i) : ?>
                            <tr>
                                <td><?php echo htmlspecialchars($prod_reali['negozzi']['posizioni'][$i]);   ?></td>
                                <td><?php echo htmlspecialchars($prod_reali['negozzi']['disp'][$i]);        ?></td>
                                <td><?php echo htmlspecialchars($prod_reali['negozzi']['sconti'][$i] ?? 0); ?>%</td>
                            </tr>
                        <?php endfor; ?>
                    </table>
                <?php else: ?>
                    <p style="color:red;font-size:medium;">Prodotto attualmente non disponibile nei negozzi</p>
                <?php endif; ?>
            </div>
            <?php 
                if(utente_collegato() && count($prod_reali['online']['disp']) > 0 && $prod_reali['online']['disp'][0] !== NULL): 
                    $disp_online_totale = 0;
                    foreach ($prod_reali['online']['disp'] as $disp) {
                        $disp_online_totale += $disp;
                    }
                ?>
 
                    <form action="./ordina.php?p=<?php echo htmlspecialchars($codice_prodotto); ?>" method="post">
                        <label for="num_prodotti">Numero di prodotti</label>
                        <input type="number" value="1" min="1" max="<?php echo $disp_online_totale; ?>" name="num_prodotti" id="num_prodotti">
                        <input type="submit" value="Ordina">
                    </form>

                    <a href="#" id="aggiungi_a_carrello">Aggiungi al carrello</a>

                 <?php endif; ?>
            <a href="./prodotti.php<?php if(strlen($pg_ricerca) > 0) echo "?pg=".htmlspecialchars($pg_ricerca); ?>">Indietro</a>
        </div>
        <script>
            document.getElementById('aggiungi_a_carrello').addEventListener(
                'click', 
                () => {
                    document.location.href="./carrello.php?p=<?php echo htmlspecialchars($codice_prodotto); ?>&q="+document.getElementById('num_prodotti').value
                }
            );
        </script>
    </body>
</html>

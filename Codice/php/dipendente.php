<?php
	session_start();
	require_once "util.php";

	if(!utente_collegato() || (!ha_permesso('D') && !ha_permesso('DC'))){
		header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', true, 404);
		die();
	}

	require_once "connessioneDB.php"; ?>
<html>
	<head>
		<title>Area Dipendente</title>
	</head>
	<body>
		<?php if(ha_permesso('A') && !isset($_GET['a'])): ?>

			<a href="dipendente.php?a=d">Dipendente</a>
			<a href="dipendente.php?a=dc">Dipendente magazzino centrale</a>
		<?php endif;
			if((isset($_POST['agg']) || isset($_POST['rm'])) && (($_SESSION['rand'] ?? -2) == ($_POST['rc'] ?? -1))){
                $_SESSION['rand'] = rand();

				if(isset($_POST['p']) && isset($_POST['modifica']) && (isset($_POST['mc']) || isset($_POST['m']))){//Modifica della quant. di prodotti
					$m_mc = isset($_POST['mc']) ? 'DC' : (isset($_POST['m']) ? 'D' : false);
					$tabella = $m_mc === 'DC' ? 'magazzino_centrale' : ($m_mc === 'D' ? 'magazzino' : false);
					$id_m_mc = ha_permesso('A') ? ($_POST['id_m_mc'] ?? false) : false; //Solo per admin 
					$id_m_mc_bind = $id_m_mc ? $id_m_mc : $_SESSION["id_$tabella"];
					$op = isset($_POST['agg']) ? '+' : (isset($_POST['rm']) ? '-' : false);
					if(ha_permesso($m_mc) && $m_mc && $tabella && $op){
						$stmt = null;
						if(
							($stmt = $conn->prepare(
								"UPDATE prodotto_$tabella
								SET prodotto_$tabella.quant = prodotto_$tabella.quant $op ?
								WHERE prodotto_$tabella.codice_prodotto = ? AND
									  prodotto_$tabella.id_$tabella = ?"
							)) === false ||
							$stmt->bind_param('isi', $_POST['modifica'], $_POST['p'], $id_m_mc_bind) === false ||
							$stmt->execute() === false
						){ ?>
							<p class="error">Errore generale!</p>
						<?php
						}
					}
				}
			}elseif(isset($_POST['spedisci']) && (($_SESSION['rand'] ?? -2) == ($_POST['rc'] ?? -1))){
                $_SESSION['rand'] = rand();
				if(isset($_POST['a_m_mc']) && isset($_POST['id_a_m_mc']) && isset($_POST['p']) && isset($_POST['quant']) && isset($_POST['data_consegna'])){ //Spedizione prodotto da mc/m a mc/m
					$data_consegna = $_POST['data_consegna'];
					$a_m_mc = isset($_POST['a_m_mc']) ? (($_POST['a_m_mc'] === 'm' || $_POST['a_m_mc'] === 'mc') ? $_POST['a_m_mc'] : false) : false;
					$id_a_m_mc = isset($_POST['id_a_m_mc']) && is_numeric($_POST['id_a_m_mc']) ? $_POST['id_a_m_mc'] : false;

					$dip_m_mc = isset($_SESSION['id_magazzino_centrale']) ? 'mc' : (isset($_SESSION['id_magazzino']) ? 'm' : false);
					$dip_id_m_mc = $dip_m_mc === 'mc' ? $_SESSION['id_magazzino_centrale'] : ($dip_m_mc === 'm' ? $_SESSION['id_magazzino'] : false);

					$da_m_mc =  ha_permesso('A') ? ($_POST['da_m_mc'] ?? false) : $dip_m_mc;
					$id_da_m_mc = ha_permesso('A') ? ($_POST['da_m_mc'] ?? false) : $dip_id_m_mc;
					$da_m_mc_permesso = $da_m_mc === 'mc' ? 'DC' : ($da_m_mc === 'm' ? 'D' : false);

					$id_a_m_mc = intval($id_a_m_mc);
					
					$da_m_mc_tabella = $da_m_mc === 'mc' ? 'magazzino_centrale' : ($da_m_mc === 'mc' ? 'magazzino' : false);

					$tabella = "{$da_m_mc}_$a_m_mc";

					if(ha_permesso($da_m_mc_permesso) && $a_m_mc && $id_a_m_mc && $da_m_mc && $id_da_m_mc && $tabella && $da_m_mc_tabella){
						$errore = false;
						$conn->query('SET SESSION sql_mode="STRICT_TRANS_TABLES"');
						{//Begin of transaction
							$conn->begin_transaction();
							$stmt = null;
							$stato = 'in_trans';
							$q = intval($_POST['quant']);
							if(
								($stmt = $conn->prepare(
									"INSERT INTO prodotto_in_transito(id, codice_prodotto, quant, data_consegna, stato)
									VALUES(0, ?, ?, ?, ?)"
								)) === false ||
								$stmt->bind_param('siss', $_POST['p'], $q, $data_consegna, $stato) === false ||
								$stmt->execute() === false
							){ 
								$errore = true;
							?>
								<p class="error">Errore generale!</p>
							<?php
							}
							if(!$errore){
								$last_id = $conn->insert_id;
								$stmt = null;
								if(
									($stmt = $conn->prepare(
										"INSERT INTO prodotto_in_transito_$tabella(prodotto_in_transito, {$da_m_mc}_part, {$a_m_mc}_dest)
										VALUES(?, ?, ?)"
									)) === false ||
									$stmt->bind_param('iii', $last_id, $id_da_m_mc, $id_a_m_mc) === false ||
									$stmt->execute() === false
								){ 
									$errore = true;
									$conn->rollback();
									$conn->query('SET SESSION sql_mode=""');//Exit strict mode
								?>
									<p class="error">Errore generale!2</p>
								<?php
								}
							}
							if(!$errore){
								$stmt = null;
								if(
									($stmt = $conn->prepare(
										"UPDATE prodotto_$da_m_mc_tabella
										SET prodotto_$da_m_mc_tabella.quant = prodotto_$da_m_mc_tabella.quant - ?
										WHERE prodotto_$da_m_mc_tabella.codice_prodotto = ? AND
											  prodotto_$da_m_mc_tabella.id_$da_m_mc_tabella = ?"
									)) === false ||
									$stmt->bind_param('isi', $_POST['quant'], $_POST['p'], $id_da_m_mc) === false ||
									$stmt->execute() === false
								){
									$errore = true;

									if(sql_out_of_range($conn->errno)):
									?>
										<p class="error">Non sono disponibili abbastanza prodotti</p>
									<?php
									else:
									?>
										<p class="error">Errore generale!3</p>
									<?php
									endif;
									$conn->rollback();
									$conn->query('SET SESSION sql_mode=""');//Exit strict mode
								}
							}
							if(!$errore){
								$conn->commit();
								$conn->query('SET SESSION sql_mode=""');//Exit strict mode
							}
						}// End of transaction
					}
				}
			}elseif(isset($_POST['aggiungi']) && (($_SESSION['rand'] ?? -2) == ($_POST['rc'] ?? -1))){
                $_SESSION['rand'] = rand();
				if(isset($_POST['p']) && isset($_POST['quant'])){
					var_dump($_SESSION);
					$tabella = isset($_SESSION['id_magazzino_centrale']) ? 'magazzino_centrale' : (isset($_SESSION['id_magazzino']) ? 'magazzino' : false);
					$id_mc = (ha_permesso('A') ? $_POST['id_mc'] : null) ?? $_SESSION['id_magazzino_centrale'] ?? $_SESSION['id_magazzino'] ?? false;
					$id_mc = intval($id_mc);
					$sconto = intval($_POST['sconto'] ?? 0);
					$quant = is_numeric($_POST['quant']) ? intval($_POST['quant']) : false;
					if($tabella && $id_mc && $quant){
						if(
							($stmt = $conn->prepare(
								"INSERT INTO prodotto_$tabella(codice_prodotto, id_$tabella, quant, sconto)
								VALUES(?, ?, ?, ?)"
							)) === false ||
							$stmt->bind_param('siii', $_POST['p'], $id_mc, $quant, $sconto) === false ||
							$stmt->execute() === false
						){ 
							$errore = true;
						?>
							<p class="error">Errore generale!</p>
						<?php
						}
					}
				}
			}elseif(isset($_POST['ritira']) && (($_SESSION['rand'] ?? -2) == ($_POST['rc'] ?? -1))){ //Ritira ordine spedito da un magazzino centrale ad un magazzino
                $_SESSION['rand'] = rand();
				if(isset($_POST['id_ord']) && is_numeric($_POST['id_ord'])){
					//Informazioni ordine
					$stmt = $inv = null;
					($stmt = $conn->prepare(
						"SELECT 
							ordine_a_magazzino_centrale.id, 
							prodotto_ordine_a_magazzino_centrale.quant, 
							prodotto_ordine_a_magazzino_centrale.codice_prodotto,
							ordine_a_magazzino_centrale.id_magazzino
						FROM ordine_a_magazzino_centrale
						INNER JOIN prodotto_ordine_a_magazzino_centrale ON(ordine_a_magazzino_centrale.id = prodotto_ordine_a_magazzino_centrale.id_ordine_a_magazzino_centrale)
						WHERE ordine_a_magazzino_centrale.id = ?
						LIMIT 1"
					)) === false ||
					$stmt->bind_param('i', $_POST['id_ord']) === false ||
					$stmt->execute() === false;
					$dettagli_ordine = $stmt->get_result()->fetch_assoc();

					{//Begin of transaction
						$errore = false;
						$conn->begin_transaction();
						if(
							($stmt = $conn->prepare(
								"UPDATE ordine_a_magazzino_centrale
								SET ordine_a_magazzino_centrale.completato = 1
								WHERE ordine_a_magazzino_centrale.id = ?"
							)) === false ||
							$stmt->bind_param('i', $_POST['id_ord']) === false ||
							$stmt->execute() === false
						){
							$errore = true;
							?>
								<p class="error">Errore generale!1</p>
							<?php
						}
						if(!$errore){
							$stmt = $inv = null;
							($stmt = $conn->prepare(
								"SELECT prodotto_magazzino.codice_prodotto
								 FROM prodotto_magazzino
								 WHERE prodotto_magazzino.codice_prodotto = ? AND 
								 	   prodotto_magazzino.id_magazzino = ?
								LIMIT 1"
							)) === false ||
							$stmt->bind_param('ii', $dettagli_ordine['codice_prodotto'], $dettagli_ordine['id_magazzino']) === false ||
							$stmt->execute() === false;
							$insert_needed = $stmt->get_result()->num_rows !== 1;
							if($insert_needed){ //Inserisci il nuovo prodotto
								$sconto = 0;
								if(
									($stmt = $conn->prepare(
										"INSERT INTO prodotto_magazzino(codice_prodotto, id_magazzino, sconto, quant)
										 VALUES (?, ?, ?, ?)"
									)) === false ||
									$stmt->bind_param('siii', $dettagli_ordine['codice_prodotto'], $dettagli_ordine['id_magazzino'], $sconto, $dettagli_ordine['quant']) === false ||
									$stmt->execute() === false
								){
									$conn->rollback();
									$errore = true;
									?>
										<p class="error">Errore generale!2</p>
									<?php
								}
							}else{//Aggiorna la quantità di prodotti già presente
								if(
									($stmt = $conn->prepare(
										"UPDATE prodotto_magazzino
										 SET prodotto_magazzino.quant = prodotto_magazzino.quant + ?
										 WHERE prodotto_magazzino.codice_prodotto = ? AND 
								 	   	 	   prodotto_magazzino.id_magazzino = ? "
									)) === false ||
									$stmt->bind_param('isi', $dettagli_ordine['quant'], $dettagli_ordine['codice_prodotto'], $dettagli_ordine['id_magazzino']) === false ||
									$stmt->execute() === false
								){
									$conn->rollback();
									$errore = true;
									?>
										<p class="error">Errore generale!3</p>
									<?php
								}
							}
						}
						if(!$errore){
							$conn->commit();	
						}
					} //End of transaction
				}
			}

			if((ha_permesso('D') && !ha_permesso('A')) || (ha_permesso('A') && ($_GET['a'] ?? '') === 'd')): //Dipendente punto vendita
				if(isset($_GET['act']) && $_GET['act'] === 'ord'): //Visualizza ordini del proprio punto vendita
					$where_sql = "";
					if(!ha_permesso('A')){
						$where_sql = 'WHERE ordine_a_magazzino_centrale.id_magazzino = ?';
					}

					$stmt = $inv = null;
					($stmt = $conn->prepare(
						"SELECT
							prodotto_ordine_a_magazzino_centrale.id_ordine_a_magazzino_centrale,
							magazzino.id as 'm_id',
							magazzino_centrale.id as 'mc_id',
							magazzino_centrale.posizione as 'mc_pos',
							magazzino.responsabile as 'mc_resp',
							prodotto.codice_a_barre as 'p_cod',
							prodotto.nome as 'p_nome',
							prodotto_ordine_a_magazzino_centrale.quant as 'quant',
							ordine_a_magazzino_centrale.data_ordine,
							ordine_a_magazzino_centrale.completato
						FROM prodotto
						INNER JOIN prodotto_ordine_a_magazzino_centrale ON(prodotto.codice_a_barre = prodotto_ordine_a_magazzino_centrale.codice_prodotto)
						INNER JOIN ordine_a_magazzino_centrale ON(prodotto_ordine_a_magazzino_centrale.id_ordine_a_magazzino_centrale = ordine_a_magazzino_centrale.id)
						INNER JOIN magazzino ON(ordine_a_magazzino_centrale.id_magazzino = magazzino.id)
						INNER JOIN magazzino_centrale ON(ordine_a_magazzino_centrale.id_magazzino_centrale = magazzino_centrale.id)
						$where_sql 
						ORDER BY ordine_a_magazzino_centrale.data_ordine DESC"
					)) === false ||
					(strlen($where_sql) > 0 ? $stmt->bind_param('i', $_SESSION['id_magazzino']) : true ) === false ||
					$stmt->execute() === false;
					$inv = $stmt->get_result();
					close_conn();
					
					//Stampa gli ordini
					?>
					<h1>Ordini</h1>
					<table border="1">
						<tr>
							<th>Id Ordine</th>
							<th>Id Magazzino</th>
							<th>Id Magazzino Centrale</th>
							<th>Posizione Magazzino Centrale</th>
							<th>Responsabile Magazzino Centrale</th>
							<th>Codice a Barre</th>
							<th>Nome Prodotto</th>
							<th>Quantit&agrave;</th>
							<th>Data Ordine</th>
							<th>Merce Ricevuta</th>
						</tr>
					<?php while($prod = $inv->fetch_assoc()): ?>
						<tr>
							<td><?php echo $prod['id_ordine_a_magazzino_centrale']; ?></td>
							<td><?php echo $prod['m_id']; ?></td>
							<td><?php echo $prod['mc_id']; ?></td>
							<td><?php echo $prod['mc_pos']; ?></td>
							<td><?php echo $prod['mc_resp']; ?></td>
							<td><?php echo $prod['p_cod']; ?></td>
							<td><?php echo $prod['p_nome']; ?></td>
							<td><?php echo $prod['quant']; ?></td>
							<td><?php echo $prod['data_ordine']; ?></td>
							<td> <?php
								if($prod['completato']){
									echo 'Si';
								}else{ ?>
									No,
									<form action="./dipendente.php?<?php if(isset($_GET['a'])) echo 'a=d&'; ?>act=ord" method="post">
										<input type="hidden" name="id_ord" value="<?php echo $prod['id_ordine_a_magazzino_centrale']; ?>">
										<input type="hidden" name="rc" value="<?php echo $_SESSION['rand']; ?> ">
										<input type="submit" name="ritira" value="Completa" />
									</form>
									<?php
								}
							?>
							</td>
						</tr>
					<?php
					endwhile;
					echo '</table>'; ?>
					<a href="./dipendente.php<?php if(isset($_GET['a'])) echo '?a=d&'; ?>">Indietro</a>
					<?php
				elseif(isset($_GET['act']) && $_GET['act'] === 'nord'): //Fai ordini verso dei magazzini centrali
					if(isset($_POST['ordina'])): //Effettua l'ordine
						if(
							!isset($_POST['codice_prodotto']) ||
							!isset($_POST['quant']) || !is_numeric($_POST['quant']) ||
							!isset($_POST['id_magazzino']) || !is_numeric($_POST['id_magazzino']) || 
							!isset($_POST['id_magazzino_centrale']) || !is_numeric($_POST['id_magazzino_centrale'])
						){
							echo '<p class="error">Inserisci tutti i dati. Torna indietro per rieffettuare l\'inserimento</p>';
							close_conn();
							die();
						}

						$codice_prodotto = $_POST['codice_prodotto'];
						$quant = $_POST['quant'];
						$id_m = $_POST['id_magazzino'];
						$id_mc = $_POST['id_magazzino_centrale'];
						$data = date("Y-m-d");
						$errore = false;
						{//Insert Transaction
							$conn->begin_transaction();
							$stmt = null;
							if(
								($stmt = $conn->prepare(
									"INSERT INTO ordine_a_magazzino_centrale
										(
											id_magazzino,
											id_magazzino_centrale,
											data_ordine
										) 
									VALUES(?, ?, ?)")) === false ||
								$stmt->bind_param('iis', $id_m, $id_mc, $data) === false ||
								$stmt->execute() === false
							){
								echo '<p class="error">Errore generale1</p>';
								$errore = true;
							}
							if(!$errore){
								$last_id = $conn->insert_id;
								$stmt->free_result();
								$stmt = null;
								if(
									($stmt = $conn->prepare(
										"INSERT INTO prodotto_ordine_a_magazzino_centrale
											(
												id_ordine_a_magazzino_centrale,
												codice_prodotto,
												quant
											) 
										VALUES(?, ?, ?)")) === false ||
									$stmt->bind_param('isi', $last_id, $codice_prodotto, $quant) === false ||
									$stmt->execute() === false
								){
									$conn->rollback();
									$conn->query('SET SESSION sql_mode=""');//Exit strict mode
									echo '<p class="error">Errore generale2</p>';
									$errore = true;
								}
								if(!$errore){
									$stmt = null;
									$conn->query('SET SESSION sql_mode="STRICT_TRANS_TABLES"');
									if(
										($stmt = $conn->prepare(
											"UPDATE prodotto_magazzino_centrale
											SET prodotto_magazzino_centrale.quant = prodotto_magazzino_centrale.quant - ?
											WHERE prodotto_magazzino_centrale.codice_prodotto = ? AND
												  prodotto_magazzino_centrale.id_magazzino_centrale = ?")) === false ||
										$stmt->bind_param('isi', $quant, $codice_prodotto, $id_mc) === false ||
										$stmt->execute() === false
									){
										if(sql_out_of_range($conn->errno)):
										?>
											<p class="error">Non sono disponibili abbastanza prodotti</p>
										<?php
										else:
										?>
											<p class="error">Errore generale3!</p>
										<?php
										endif;
										$conn->rollback();
										$conn->query('SET SESSION sql_mode=""');//Exit strict mode
										$errore = true;
									}
								}
								if(!$errore){
									$conn->commit();
									$conn->query('SET SESSION sql_mode=""');//Exit strict mode
								}
							}
						}//End of transaction
						if(!$errore):
							?>
							<h1>Ordine completato</h1>
							<p>Riepilogo</p>
							<table border="1">
								<tr><th>Codice Prodotto</th><td><?php echo $codice_prodotto; ?></td></tr>
								<tr><th>Quantit&agrave;</th><td><?php echo $quant; ?></td></tr>
								<tr><th>Id Magazzino</th><td><?php echo $id_m; ?></td></tr>
								<tr><th>Id Magazzino Centrale</th><td><?php echo $id_mc; ?></td></tr>
							</table>
							<?php
						else: ?>
							<p>Controlla la correttezza dei dati inseriti</p>
						<?php endif;
					endif;
					?>
						<form action="" method="post">
							<label for="codice_prodotto">Codice Prodotto</label>
							<input type="text" name="codice_prodotto" id="codice_prodotto" required/>
							<br>
							<label for="quant">Quantit&agrave;</label>
							<input type="number" name="quant" id="quant" min="1" value="1" required/>
							<br>
							<?php if(isset($_SESSION['id_magazzino']) && $_SESSION['id_magazzino'] != null): ?>
								<input type="hidden" name="id_magazzino" value="<?php echo $_SESSION['id_magazzino']; ?>" required/>
							<?php elseif(ha_permesso('A')): ?>
								<label for="id_magazzino">Id Magazzino</label>
								<input type="number" name="id_magazzino" id="id_magazzino" required/>
								<br>
							<?php endif; ?>
							<label for="id_magazzino_centrale">ID Magazzino Centrale</label>
							<input type="number" name="id_magazzino_centrale" id="id_magazzino_centrale"/>
							<p style="color:red;">La possibilit&agrave; di inserire pi&ugrave; articoli nello stesso ordine sar&agrave; presto disponibile!</p>
							<br>
							<input type="submit" name="ordina" value="Ordina">
						</form>
						<a href="./index.php">Home</a>
						<a href="./dipendente.php<?php if(isset($_GET['a'])) echo '?a=d&'; ?>">Indietro</a>
					<?php
				else: //Visualizza inventario --Home per il dipendente--
					$where_sql = "";
					if(!ha_permesso('A')){
						$where_sql = 'WHERE prodotto_magazzino.id_magazzino = ?';
					}
					$stmt = $inv = null;
					($stmt = $conn->prepare(
						"SELECT 
							prodotto.codice_a_barre, prodotto.nome, prodotto_magazzino.quant as 'disp', 
							prodotto_magazzino.id_magazzino
						FROM prodotto
						INNER JOIN prodotto_magazzino ON(prodotto.codice_a_barre = prodotto_magazzino.codice_prodotto)
						$where_sql 
						ORDER BY prodotto.nome ASC"
					)) === false ||
					(strlen($where_sql) > 0 ? $stmt->bind_param('i', $_SESSION['id_magazzino']) : true) === false ||
					$stmt->execute() === false;
					$inv = $stmt->get_result();
					close_conn();

					//Stampa l'inventaio
					?>
					<h1>Inventario</h1>
					<table border="1">
						<tr>
							<th>Id Magazzino</th>
							<th>Codice a Barre</th>
							<th>Nome Prodotto</th>
							<th>Quantit&agrave;</th>
							<th>Aggiorna</th>
						</tr>
					<?php
					while($prod = $inv->fetch_assoc()): ?>

						<tr>
							<td><?php echo $prod['id_magazzino'] ?></td>
							<td><?php echo $prod['codice_a_barre'] ?></td>
							<td><?php echo $prod['nome'] ?></td>
							<td <?php if($prod['disp'] <= 0) echo 'class="none-left"'; ?> ><?php echo $prod['disp'] ?></td>
						<td>
							<form action="" method="post">
								<input type="number" name="modifica" />
								<input type="hidden" name="m"/>
								<input type="hidden" name="id_m_mc" value="<?php echo $prod['id_magazzino']; ?>">
								<input type="hidden" name="p" value="<?php echo $prod['codice_a_barre']; ?>"/>
								<input type="hidden" name="rc" value="<?php echo $_SESSION['rand']; ?> ">
								<input type="submit" name="agg" value="Aggiungi" />
								<input type="submit" name="rm" value="Rimuovi" />
							</form>
						</td>
					</tr>
						</tr>
					<?php
					endwhile;
					echo '</table>'; ?>
					<a href="./index.php">Home</a>
					<a href="./dipendente.php?<?php if(isset($_GET['a'])) echo 'a=d&'; ?>act=ord">Ordini</a>
					<a href="./dipendente.php?<?php if(isset($_GET['a'])) echo 'a=d&'; ?>act=nord">Ordina Prodotti</a>
					<?php
				endif;
			elseif((ha_permesso('DC') && !ha_permesso('A')) || (ha_permesso('A') && ($_GET['a'] ?? '') === 'dc')): //Dipendente magazzino centrale
				if(isset($_GET['act']) && $_GET['act'] === 'ord'): //Visualizza ordini verso il proprio magazzino centrale
					$where_sql = "";
					if(!ha_permesso('A')){
						$where_sql = 'WHERE ordine_a_magazzino_centrale.id_magazzino_centrale = ?';
					}

					$stmt = $inv = null;
					($stmt = $conn->prepare(
						"SELECT
							magazzino.id as 'm_id',
							magazzino.posizione as 'm_pos',
							magazzino.responsabile as 'm_resp',
							prodotto.codice_a_barre as 'p_cod',
							prodotto.nome as 'p_nome',
							prodotto_ordine_a_magazzino_centrale.quant as 'quant',
							ordine_a_magazzino_centrale.data_ordine
						FROM prodotto
						INNER JOIN prodotto_ordine_a_magazzino_centrale ON(prodotto.codice_a_barre = prodotto_ordine_a_magazzino_centrale.codice_prodotto)
						INNER JOIN ordine_a_magazzino_centrale ON(prodotto_ordine_a_magazzino_centrale.id_ordine_a_magazzino_centrale = ordine_a_magazzino_centrale.id)
						INNER JOIN magazzino ON(ordine_a_magazzino_centrale.id_magazzino = magazzino.id)
						$where_sql
						ORDER BY ordine_a_magazzino_centrale.data_ordine DESC"
					)) === false ||
					(strlen($where_sql) > 0 ? $stmt->bind_param('i', $_SESSION['id_magazzino_centrale']) : true ) === false ||
					$stmt->execute() === false;
					$inv = $stmt->get_result();
					close_conn();
					
					//Stampa gli ordini
					?>
					<h1>Ordini</h1>
					<table border="1">
						<tr>
							<th>Id Magazzino</th>
							<th>Posizione Magazzino</th>
							<th>Responsabile Magazzino</th>
							<th>Codice a Barre</th>
							<th>Nome Prodotto</th>
							<th>Quantit&agrave;</th>
							<th>Data Ordine</th>
						</tr>
					<?php while($prod = $inv->fetch_assoc()): ?>
						<tr>
							<td><?php echo $prod['m_id'] ?></td>
							<td><?php echo $prod['m_pos'] ?></td>
							<td><?php echo $prod['m_resp'] ?></td>
							<td><?php echo $prod['p_cod'] ?></td>
							<td><?php echo $prod['p_nome'] ?></td>
							<td><?php echo $prod['quant'] ?></td>
							<td><?php echo $prod['data_ordine'] ?></td>
						</tr>
					<?php
					endwhile;
					echo '</table>'; ?>
					<a href="./index.php">Home</a>
					<a href="./dipendente.php<?php if(isset($_GET['a'])) echo '?a=dc'; ?>">Indietro</a>
					<?php
				elseif(isset($_GET['act']) && $_GET['act'] === 'trans'): //Spedisci un prodotto
					?>
					<h1>Spedisci prodotto</h1>
					<form action="" method="post">
						<?php if(ha_permesso('A')): ?>
							<label for="da_m_mc">Da:</label>
							<select name="da_m_mc" id="da_m_mc">
								<option value="m">Magazzino</option>
								<option value="mc">Magazzino Centrale</option>
							</select>
							<input type="number" name="id_da_m_mc" />
						<?php endif;?>

						<label for="a_m_mc">A:</label>
						<select name="a_m_mc" id="a_m_mc">
							<option value="m">Magazzino</option>
							<option value="mc">Magazzino Centrale</option>
						</select>
						<br>
						<label for="id_a_m_mc">Id:</label>
						<input type="number" name="id_a_m_mc" id="id_a_m_mc" />
						<br>
						<label for="p">Codice a barre:</label>
						<input type="text" name="p" id="p" />
						<br>
						<label for="quant">Quantit&agrave;:</label>
						<input type="number" name="quant" id="quant" />
						<br>
						<label for="data_consegna">Data consegna:</label>
						<input type="date" name="data_consegna" id="data_consegna"/>
						<br>
						<input type="hidden" name="rc" value="<?php echo $_SESSION['rand']; ?> " />
						<input type="submit" name="spedisci" value="Spedisci" />
					</form>
					<a href="./index.php">Home</a>
					<a href="./dipendente.php<?php if(isset($_GET['a'])) echo '?a=dc'; ?>">Indietro</a>
				<?php	
				elseif(isset($_GET['act']) && $_GET['act'] === 'agg'): //Aggiungi prodotti
					?>
					<h1>Aggiungi prodotto</h1>
					<form action="" method="post">
						<?php if(ha_permesso('A')): ?>
							<label for="p">Id Magazzino centrale:</label>
							<input type="number" name="id_mc" id="id_mc" />
							<br>
						<?php endif; ?>
						<label for="p">Codice a barre:</label>
						<input type="text" name="p" id="p" />
						<br>
						<label for="quant">Quantit&agrave;:</label>
						<input type="number" name="quant" id="quant" />
						<br>
						<label for="sconto">Sconto:</label>
						<input type="number" name="sconto" id="sconto"/>
						<br>
						<input type="hidden" name="rc" value="<?php echo $_SESSION['rand']; ?> " />
						<input type="submit" name="aggiungi" value="Aggiungi" />
					</form>
					<a href="./index.php">Home</a>
					<a href="./dipendente.php<?php if(isset($_GET['a'])) echo '?a=dc'; ?>">Indietro</a>
				<?php	
				else: //Visualizza inventario --Home per il dipendente--
					$where_sql = "";
					if(!ha_permesso('A')){
						$where_sql = 'WHERE prodotto_magazzino_centrale.id_magazzino_centrale = ?';
					}

					$stmt = $inv = null;
					($stmt = $conn->prepare(
						"SELECT prodotto.codice_a_barre, prodotto.nome, prodotto_magazzino_centrale.quant as 'disp', prodotto_magazzino_centrale.id_magazzino_centrale
						FROM prodotto
						INNER JOIN prodotto_magazzino_centrale ON(prodotto.codice_a_barre = prodotto_magazzino_centrale.codice_prodotto)
						$where_sql"
					)) === false ||
					(strlen($where_sql) > 0 ? $stmt->bind_param('i', $_SESSION['id_magazzino_centrale']) : true) === false ||
					$stmt->execute() === false;
					$inv = $stmt->get_result();
					close_conn();
					//Stampa l'inventaio
					?>
					<h1>Inventario</h1>
					<table border="1">
						<tr>
							<th>Id Magazzino Centrale</th>
							<th>Codice a Barre</th>
							<th>Nome Prodotto</th>
							<th>Disponibilit&agrave;</th>
							<th>Aggiorna Dati</th>
						</tr>
					<?php

					while($prod = $inv->fetch_assoc()): ?>

						<tr>
							<td><?php echo $prod['id_magazzino_centrale'] ?></td>
							<td><?php echo $prod['codice_a_barre'] ?></td>
							<td><?php echo $prod['nome'] ?></td>
							<td <?php if($prod['disp'] <= 0) echo 'class="none-left"'; ?> ><?php echo $prod['disp'] ?></td>
							<td>
								<form action="" method="post">
									<input type="number" name="modifica" />
									<input type="hidden" name="mc"/>
									<input type="hidden" name="id_m_mc" value="<?php echo $prod['id_magazzino_centrale']; ?>">
									<input type="hidden" name="p" value="<?php echo $prod['codice_a_barre']; ?>"/>
									<input type="hidden" name="rc" value="<?php echo $_SESSION['rand']; ?> ">
									<input type="submit" name="agg" value="Aggiungi" />
									<input type="submit" name="rm" value="Rimuovi" />
								</form>
							</td>
						</tr>
					<?php
					endwhile; ?>
					</table>
					<a href="./index.php">Home</a>
					<a href="./dipendente.php?<?php if(isset($_GET['a'])) echo 'a=dc&'; ?>act=ord">Ordini</a>
					<a href="./dipendente.php?<?php if(isset($_GET['a'])) echo 'a=dc&'; ?>act=trans">Spedisci a Magazzini</a>
					<a href="./dipendente.php?<?php if(isset($_GET['a'])) echo 'a=dc&'; ?>act=agg">Aggiungi prodotto</a>
					<?php
				endif;
			endif;
			close_conn();
		?>		
	</body>
</html>
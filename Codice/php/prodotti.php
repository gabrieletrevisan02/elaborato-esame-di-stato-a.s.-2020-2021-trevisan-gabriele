<html>
    <head>
        <title>Prodotti</title>
    </head>
    <body>
      <a href="./index.php">Home</a>
        <h1>Lista prodotti</h1>
        <div>
            <?php
                require_once "connessioneDB.php";
                require_once "util.php";

                const PER_PAGINA = 20;
                $pagina = $_GET['pg'] ?? 0;

                $stmt = $conn->prepare(
                    "SELECT
                        prodotto.codice_a_barre, prodotto.nome AS 'p_nome',
                        prodotto.prezzo_finale, reparto.nome AS 'r_nome', fornitore.nome AS 'f_nome',
                        magazzino_centrale.posizione, SUM(prodotto_magazzino_centrale.quant) as 'disp',
                        prodotto.vendibile_online
                     FROM prodotto
                     LEFT JOIN prodotto_magazzino_centrale ON(prodotto.codice_a_barre = prodotto_magazzino_centrale.codice_prodotto)
                     LEFT JOIN magazzino_centrale ON(prodotto_magazzino_centrale.id_magazzino_centrale = magazzino_centrale.id)
                     INNER JOIN reparto ON(prodotto.id_reparto = reparto.id)
                     INNER JOIN fornitore ON(prodotto.id_fornitore = fornitore.id)
                     GROUP BY prodotto.codice_a_barre
                     LIMIT ?, ?"
                );
                $da = PER_PAGINA * $pagina;
                $a = $da + PER_PAGINA + 1;
                $stmt->bind_param('ii', $da, $a) === FALSE ||
                $stmt->execute();
                $result = $stmt->get_result();
                $i = 0;
                $altri = $result->num_rows >= PER_PAGINA+1;

                while(($prod = $result->fetch_assoc()) && $i < PER_PAGINA): ?>

                    <div>
                        <a href="./prodotto.php?p=<?php echo $prod['codice_a_barre']; if($pagina !== 0) echo '&pg_r=' . htmlspecialchars($pagina); ?>">
                            <h2><?php echo htmlspecialchars($prod['p_nome']); ?></h2>
                        </a>
                        <?php if($prod['vendibile_online'] == FALSE): ?>
                            <p style="color:red;font-size:medium;">Prodotto non disponibile online</p>
                        <?php endif; ?>
                        <p><?php  echo htmlspecialchars($prod['codice_a_barre']); ?></p>
                        <p><?php  echo htmlspecialchars(number_format(round($prod['prezzo_finale'] / 100.0, 2), 2, ',', '')); ?>&euro;</p>
                        <p><?php  echo htmlspecialchars($prod['r_nome']); ?></p>
                        <p>Fornitore: <?php  echo htmlspecialchars($prod['f_nome']); ?></p>
                        <?php if($prod['disp'] != NULL && $prod['disp'] != 0): ?>
                            <p>Disponibilit&agrave;: <?php  echo htmlspecialchars($prod['disp']); ?></p>
                        <?php else: ?>
                            <p style="color:red;font-size:medium;">Prodotto attualmente non disponibile online</p>
                        <?php endif; ?>
                    </div>

                <?php
                    $i++;
                endwhile;
                close_conn();
                if($pagina != 0): ?>
                    <a href="./prodotti.php?pg=<?php echo $pagina - 1; ?>" >Indietro</a>
                <?php endif;
                if($altri): ?>
                    <a href="./prodotti.php?pg=<?php echo $pagina + 1; ?>" >Avanti</a>
                <?php endif;
            ?>
        </div>
    </body>
</html>

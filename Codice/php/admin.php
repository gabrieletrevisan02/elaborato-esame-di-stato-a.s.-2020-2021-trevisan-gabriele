<?php
    session_start();
    require_once "util.php";
    if(!utente_collegato() || !ha_permesso('A')){
        header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', true, 404);
        die();
    } 
?>
<html>
    <head>
        <title>Area Amministrativa</title>
    </head>
    <body>
        <?php
        if(isset($_GET['act']) && (($_SESSION['rand'] ?? -2) == ($_POST['rc'] ?? -1))){
            $_SESSION['rand'] = rand(); 
            if($_GET['act'] === 'agg' && isset($_POST['t'])){//Aggiungi dipendente
                if(isset($_POST['email'])){
                    $tipo = $_POST['t'];
                    $email = $_POST['email'];
                    require_once 'connessioneDB.php';
                    $errore = true;
                    $errore = !(
                            (
                                $tipo === 'd' && 
                                isset($_POST['id_m_mc']) && 
                                is_numeric($_POST['id_m_mc'])
                            ) || (
                                $tipo === 'dc' && 
                                isset($_POST['id_m_mc']) && 
                                is_numeric($_POST['id_m_mc'])
                            )
                        );
                    $d_dc = "'D'";
                    $m_mc = 'dipendente_magazzino';
                    if($tipo === 'dc'){
                        $d_dc = "'DC'";
                        $m_mc = 'dipendente_magazzino_centrale';
                    }elseif($tipo !== 'd'){
                        $errore = true;
                    }
                    
                    $id_utente = null;
                    $stmt = null;
                    if(
                        ($stmt = $conn->prepare(
                            "SELECT utente.id
                            FROM utente
                            WHERE utente.mail = ?"
                        )) === false ||
                        $stmt->bind_param('s', $email) === false ||
                        $stmt->execute() === false
                    ){
                        echo '<p class="error">Errore generale</p>';
                        $errore = true;
                    }
                    $res = $stmt->get_result();
                    if($res->num_rows === 1){
                        $id_utente = $res->fetch_array()[0];
                    }else{
                        echo '<p class="error">Errore generale</p>';
                        $errore = true;
                    }
                    $id_magazzino = $_POST['id_m_mc'];

                    if(!$errore){
                        $conn->begin_transaction();
                        { //Begin of transaction
                            $stmt = null;
                            if(
                                ($stmt = $conn->prepare(
                                    "UPDATE utente
                                    SET utente.permessi = $d_dc
                                    WHERE utente.mail = ?"
                                )) === false ||
                                $stmt->bind_param('s', $email) === false ||
                                $stmt->execute() === false
                            ){
                                echo '<p class="error">Errore generale</p>';
                                $errore = true;
                            }
                            if(!$errore){
                                $stmt->free_result();
                                $stmt = null;
                                if(
                                    ($stmt = $conn->prepare(
                                        "INSERT INTO $m_mc
                                        VALUES(?, ?)"
                                    )) === false ||
                                    $stmt->bind_param('ii', $id_utente, $id_magazzino) === false ||
                                    $stmt->execute() === false
                                ){
                                    echo '<p class="error">Errore generale</p>';
                                    $errore = true;
                                    rollback();
                                }
                                if(!$errore){
                                    commit();
                                }
                            }
                        } //End of transaction
                    }
                    ?>
                    <p>Dipendente "<?php echo $email; ?>" aggiunto correttamente</p>
                    <?php
                    close_conn();
                }
            }elseif($_GET['act'] === 'agg_m_mc'){
                if(isset($_POST['dir']) && isset($_POST['pos']) && isset($_POST['m_mc'])){
                    require_once "connessioneDb.php";
                    $tabella = $_POST['m_mc'] === 'm' ? 'magazzino' : ($_POST['m_mc'] === 'mc' ? 'magazzino_centrale' : false);
                    $stmt = null;
                    if($tabella){
                        if(
                            ($stmt = $conn->prepare(
                                "INSERT INTO $tabella(responsabile, posizione)
                                VALUES(?, ?)"
                            )) === false ||
                            $stmt->bind_param('ss', $_POST['dir'], $_POST['pos']) === false ||
                            $stmt->execute() === false
                        ){ 
                        ?>
                            <p class="error">Errore generale!</p>
                        <?php
                        }else{ ?>
                            <h1>Magazzino <?php echo ($_POST['m_mc'] === 'mc' ? 'centrale' : ''); ?> aggiunto correttamente</h1>
                            <p>Id nuovo magazzino <?php echo ($_POST['m_mc'] === 'mc' ? 'centrale' : ''); ?>: <?php echo $conn->insert_id; ?></p>
                            <?php
                        }
                    }
                    close_conn();
                }
            }elseif($_GET['act'] === 'agg_pv'){
                if(isset($_POST['nome']) && isset($_POST['dir']) && isset($_POST['pos']) && isset($_POST['id_mag'])){
                    require_once "connessioneDb.php";
                    $stmt = null;
                    if(
                        ($stmt = $conn->prepare(
                            "INSERT INTO supermercato(nome, direttore, posizione, id_magazzino)
                            VALUES(?, ?, ?, ?)"
                        )) === false ||
                        $stmt->bind_param('sssi', $_POST['nome'], $_POST['dir'], $_POST['pos'], $_POST['id_mag']) === false ||
                        $stmt->execute() === false
                    ){ 
                    ?>
                        <p class="error">Errore generale!</p>
                    <?php
                    }else{ ?>
                        <h1>Punto vendita aggiunto correttamente</h1>
                        <p>Id nuovo punto vendita <?php echo $conn->insert_id; ?></p>
                       <?php
                    }
                    close_conn();
                }
            }
        }
        if(isset($_GET['v'])):
            if($_GET['v'] === 'agg_d'): ?>
                <h1>Aggiungi Dipendente</h1>
                <form action="./admin.php?v=agg_d&act=agg" method="post">
                    <label for="email">E-Mail</label>
                    <input type="email" name="email" id="email" required/>
                    <br>
                    <label for="tipo">Tipologia</label>
                    <select name="t" id="tipo" required>
                        <option value="d">Dipendente Magazzino/Supermercato</option>
                        <option value="dc">Dipendente Magazzino Centrale</option>
                    </select>
                    <br>
                    <label for="id_m_mc">Id Magazzino/Magazzino Centrale</label>
                    <input type="number" name="id_m_mc" id="id_m_mc" required/>
                    <input type="hidden" name="rc" value="<?php echo $_SESSION['rand']; ?> " />
                    <input type="submit" value="Aggiungi" />
                </form>
                <a href="./index.php">Home</a>
                <a href="./admin.php">Indietro</a>
            <?php
            elseif($_GET['v'] === 'agg_m_mc'): ?>
                <h1>Aggiungi Magazzino / Magazzino Centrale</h1>
                <form action="./admin.php?v=agg_m_mc&act=agg_m_mc" method="post">
                    <label for="dir">Responsabile:</label>
                    <input type="text" name="dir" id="dir" required/>
                    <br>
                    <label for="tipo">Tipologia</label>
                    <select name="m_mc" id="m_mc" required>
                        <option value="m">Magazzino</option>
                        <option value="mc">Magazzino Centrale</option>
                    </select>
                    <br>
                    <label for="pos">Posizione Magazzino/Magazzino Centrale</label>
                    <input type="text" name="pos" id="pos" required/>
                    <input type="hidden" name="rc" value="<?php echo $_SESSION['rand']; ?> " />
                    <input type="submit" value="Aggiungi" />
                </form>
                <a href="./index.php">Home</a>
                <a href="./admin.php">Indietro</a>
            <?php
            elseif($_GET['v'] === 'agg_pv'): ?>
                <h1>Aggiungi Punto Vendita</h1>
                <form action="./admin.php?v=agg_pv&act=agg_pv" method="post">
                    <label for="nome">Nome</label>
                    <input type="text" name="nome" id="nome" required/>
                    <br>
                    <label for="dir">Direttore</label>
                    <input type="text" name="dir" id="dir" required />
                    <br>
                    <label for="pos">Posizione</label>
                    <input type="text" name="pos" id="pos" required />
                    <br>
                    <label for="id_mag">Id Magazzino</label>
                    <input type="number" name="id_mag" id="id_mag" required/>
                    <input type="hidden" name="rc" value="<?php echo $_SESSION['rand']; ?> " />
                    <input type="submit" value="Aggiungi" />
                </form>
                <a href="./index.php">Home</a>
                <a href="./admin.php">Indietro</a>
            <?php
            endif;
        else: ?>
            <h1>Area Amministrativa</h1>
            <a href="./index.php">Home</a><br>
            <a href="./admin.php?v=agg_d">Aggiungi dipendente</a><br>
            <a href="./admin.php?v=agg_m_mc">Aggiungi magazzino/magazzino centrale</a><br>
            <a href="./admin.php?v=agg_pv">Aggiungi punto vendita</a>
        <?php endif; ?>
    </body>
</html>

<?php
    require_once "credenziali_db.php";

    $conn = new mysqli($servername, $db_username, $db_password, $db_name);

    if ($conn->connect_errno) {
        die("Connessione fallita " . $conn->connect_error);
    }
    $conn->set_charset("utf8mb4_unicode_ci");

    function begin_transaction(){
        if(isset($conn)){
            $conn->begin_transaction();
            return true;
        }
        return false;
    }

    function commit(){
        if(isset($conn)){
            $conn->commit();
            return true;
        }
        return false;
    }

    function rollback(){
        if(isset($conn)){
            $conn->rollback();
            return true;
        }
        return false;
    }

    function close_conn(){
        if(isset($conn)){
            $conn->close();
        }
    }

    function close_conn_and_die(){
        close_conn();
        die();
    }
?>

<html>
    <head>
        <title>Punti Vendita</title>
    </head>
    <body>
      <a href="./index.php">Home</a>
        <h1>Punti vendita</h1>
        <div>
            <?php
                require_once "connessioneDB.php";
                require_once "util.php";

                $result = $conn->query("SELECT nome, posizione, direttore FROM supermercato");

                while($sup = $result->fetch_assoc()): ?>

                    <div>
                        <h2><?php echo htmlspecialchars($sup['nome']); ?></h2>
                        <p><?php  echo htmlspecialchars($sup['posizione']); ?></p>
                        <p><?php  echo htmlspecialchars($sup['direttore']); ?></p>
                    </div>

                <?php endwhile;
                close_conn();
            ?>
        </div>
    </body>
</html>

<?php
    session_start();
    require_once "util.php";
    if(!utente_collegato()){
        header('location: index.php');
        die();
    }
?>
<html>
    <head>
        <title>Ordina</title>
    </head>
    <body>
        <?php
        $prezzo_spedizione = 100; //TODO: Calcolare in modo migliore
        $errore = false;
        require_once 'connessioneDB.php';
        if(!$errore):
            if(isset($_POST['ordina']) && (($_SESSION['rand'] ?? -2) == ($_POST['rc'] ?? -1))):
                $_SESSION['rand'] = rand();
                if(
                    !isset($_POST['prodotti']) ||
                    !isset($_POST['ind_consegna']) ||
                    !isset($_POST['metodo_pagamento'])
                ): 
                    $errore = true; ?>
                    <p class="error">Controlla i valori inseriti!</p>
                <?php endif;
                if(!$errore):
                    $prodotti = json_decode($_POST['prodotti'], true);

                    $data_preferita_consegna = $_POST['data_preferita_consegna'] ?? '';
                    $ind_consegna = $_POST['ind_consegna'];
                    $metodo_pagamento = $_POST['metodo_pagamento'];

                    $prodotto = $stmt = $quant = $codice_prodotto = null;

                    if(
                        ($stmt = $conn->prepare(
                            "SELECT
                                magazzino_centrale.id, magazzino_centrale.posizione,
                                prodotto_magazzino_centrale.quant as 'disp', prodotto_magazzino_centrale.sconto,
                                prodotto.nome, prodotto.codice_a_barre
                            FROM prodotto
                            LEFT JOIN prodotto_magazzino_centrale ON(prodotto.codice_a_barre = prodotto_magazzino_centrale.codice_prodotto AND prodotto_magazzino_centrale.quant >= ?)
                            LEFT JOIN magazzino_centrale ON(prodotto_magazzino_centrale.id_magazzino_centrale = magazzino_centrale.id)
                            WHERE prodotto.codice_a_barre = ? AND prodotto.vendibile_online = 1
                            LIMIT 1"
                        )) === false ||
                        $stmt->bind_param('is', $quant, $codice_prodotto) === false
                    ){
                        $errore = true;
                        ?>
                            <p class="error">Errore nel caricamento del prodotto dal database</p>
                        <?php
                    }
                    
                    if(!$errore){
                        $prodotti_db = [];
                        foreach ($prodotti as $codice_prodotto => $quant) {
                            $stmt->execute() === false ||
                            ($prod = $stmt->get_result()) === false;
                    
                            if($prod->num_rows <= 0){
                                $errore = true;
                                ?>
                                    <p class="error">Errore nel caricamento del prodotto dal database</p>
                                <?php
                                break;
                            }else{
                                $res = $prod->fetch_assoc();
                                $prodotti_db[] = [
                                    'codice_a_barre' => $res['codice_a_barre'],
                                    'quant' => $prodotti[$res['codice_a_barre']], //Quant da comprare
                                    'sconto' => $res['sconto'],
                                    'id_magazzino_centrale' => $res['id'],
                                ];
                            }
                        }

                        if(!$errore){
                            $data = date('Y-m-d');
                            $stato = $metodo_pagamento === 'pp' ? 'att_pag' : 'att_lav';
                            {//Start of transaction
                                $conn->begin_transaction();
                                $stmt = null;
                                if(($stmt = $conn->prepare(
                                    "INSERT INTO ordine
                                        (
                                            id_utente, 
                                            data_ordine, 
                                            data_preferita_consegna, 
                                            ind_consegna, 
                                            prezzo_spedizione, 
                                            stato, 
                                            metodo_pagamento
                                        ) VALUES (?, ?, ?, ?, ?, ?, ?)"
                                )) === false ||
                                $stmt->bind_param(
                                    'isssiss', 
                                    $_SESSION['id'],
                                    $data,
                                    $data_preferita_consegna,
                                    $ind_consegna,
                                    $prezzo_spedizione,
                                    $stato,
                                    $metodo_pagamento
                                ) === false ||
                                $stmt->execute() === false){
                                    $errore = true;
                                    ?>
                                        <p class="error">Errore nell'effettuazione dell'ordine!</p>
                                    <?php
                                }
                                //Dichiarazione variabili per bind_param
                                $id_ordine = $conn->insert_id;
                                $id_magazzino_centrale = $codice_prodotto = $quant = null;
                                //
                                $stmt = null;
                                if(
                                    ($stmt = $conn->prepare(
                                    "INSERT INTO prodotto_ordine
                                        (
                                            id_ordine, 
                                            codice_prodotto,
                                            id_magazzino_centrale,
                                            quant
                                        ) VALUES (?, ?, ?, ?)"
                                        )
                                    ) === false ||
                                    $stmt->bind_param(
                                        'isii', 
                                        $id_ordine,
                                        $codice_prodotto,
                                        $id_magazzino_centrale,
                                        $quant
                                    ) === false
                                ){
                                    $errore = true;
                                    $conn->rollback();
                                    ?>
                                        <p class="error">Errore nell'effettuazione dell'ordine!</p>
                                    <?php
                                }

                                $stmt_dec_prod = null;
                                if(
                                    ($stmt_dec_prod = $conn->prepare(
                                        "UPDATE prodotto_magazzino_centrale
                                        SET prodotto_magazzino_centrale.quant = prodotto_magazzino_centrale.quant - ?
                                        WHERE prodotto_magazzino_centrale.codice_prodotto = ? AND
                                            prodotto_magazzino_centrale.id_magazzino_centrale = ?"
                                    )) === false ||
                                    $stmt_dec_prod->bind_param('isi', $quant, $codice_prodotto, $id_magazzino_centrale) === false
                                ){
                                    $conn->rollback();
                                    echo '<p class="error">Errore generale</p>';
                                    $errore = true;
                                }

                                foreach ($prodotti_db as $prodotto) {
                                    if(!$errore){
                                        $id_magazzino_centrale = $prodotto['id_magazzino_centrale'];
                                        $codice_prodotto = $prodotto['codice_a_barre'];
                                        $quant = $prodotto['quant'];
                                        if(!$stmt->execute()){
                                            $errore = true;
                                            $conn->rollback();
                                            ?>
                                                <p class="error">Errore nell'effettuazione dell'ordine!</p>
                                            <?php
                                        }
                                        if(!$errore){
                                            if(!$stmt_dec_prod->execute()){
                                                $conn->rollback();
                                                echo '<p class="error">Errore generale</p>';
                                                $errore = true;
                                            }
                                            if(!$errore){
                                                $conn->commit();
                                            }
                                        }
                                    }
                                }
                            }//End of transaction
                            if(!$errore): ?>
                                <h1>Ordine completato</h1>
                            <?php endif;
                        }
                    }
                endif;
            else:
                $quant = [];
                if(isset($_GET['p']) && isset($_POST['num_prodotti'])){
                    $codice_prodotto = $_GET['p'];
                    $quant[$codice_prodotto] = $_POST['num_prodotti'];
                }

                if(isset($_COOKIE['carrello'])){
                    $carrello = json_decode($_COOKIE['carrello'], true);
                    foreach ($carrello as $key => $value) {
                                                
                        $quant[$key] = $value;
                        
                        unset($key);
                    }
                }

                $q = $codice_prodotto = null;
                if(
                    ($stmt = $conn->prepare(
                        "SELECT magazzino_centrale.id, magazzino_centrale.posizione,
                        prodotto_magazzino_centrale.sconto,
                        prodotto.nome, prodotto.codice_a_barre, prodotto.prezzo_finale
                        FROM prodotto
                        LEFT JOIN prodotto_magazzino_centrale ON(prodotto.codice_a_barre = prodotto_magazzino_centrale.codice_prodotto AND prodotto_magazzino_centrale.quant >= ?)
                        LEFT JOIN magazzino_centrale ON(prodotto_magazzino_centrale.id_magazzino_centrale = magazzino_centrale.id)
                        WHERE prodotto.codice_a_barre = ? AND prodotto.vendibile_online = 1
                        LIMIT 1"
                    )) === false ||
                    $stmt->bind_param('is', $q, $codice_prodotto) === false
                ){
                    $errore = true;
                    ?>
                        <p class="error">Errore nel caricamento dei prodotti dal database</p>
                    <?php
                }
                $prodotti = [];
                foreach ($quant as $codice_prodotto => $q) {
                    $prodotto_query = null;
                    $stmt->execute() === false ||
                    ($prodotto_query = $stmt->get_result()) === false;
    
                    if($prodotto_query == null || $prodotto_query->num_rows <= 0){
                        $errore = true;
                        ?>
                            <p class="error">Errore nel caricamento dei prodotti dal database</p>
                        <?php
                        break;
                    }
                    $prodotti[] = $prodotto_query->fetch_assoc();
                }
                
                if(!$errore): ?>
                    <h1>Riepilogo ordine</h1>
                    <table border="1">
                        <tr><th>Prodotto</th><th>Luogo di partenza</th><th>Prezzo</th><th>Sconto</th><th>Prezzo Scontato</th><th>Quantit&agrave;</th><th>Totale prodotto</th></tr>
                        <?php
                            $sub_tot = 0;
                            foreach ($prodotti as $prodotto): ?>
                            <tr>
                                <td><?php echo htmlspecialchars($prodotto['nome']); ?></td>
                                <td><?php echo htmlspecialchars($prodotto['posizione']); ?></td>
                                <td><?php echo htmlspecialchars(number_format(round($prodotto['prezzo_finale'] / 100.0, 2), 2, ',', '')); ?>&euro;</td>
                                <td><?php echo htmlspecialchars($prodotto['sconto'] ?? 0); ?>%</td>
                                <?php 
                                    $prezzo_scontato = $prodotto['prezzo_finale'] - ($prodotto['prezzo_finale'] * $prodotto['sconto'] / 100);
                                    $sub_tot += $prezzo_scontato * $quant[$prodotto['codice_a_barre']];
                                ?>
                                <td><?php echo number_format(round($prezzo_scontato / 100.0, 2), 2, ',', ''); ?>&euro;</td>
                                <td><?php echo htmlspecialchars($quant[$prodotto['codice_a_barre']]); ?></td>
                                <td><?php echo number_format(round($prezzo_scontato * $quant[$prodotto['codice_a_barre']] / 100.0, 2), 2, ',', ''); ?>&euro;</td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                    <p>Spedizone: <?php echo number_format(round($prezzo_spedizione / 100.0, 2), 2, ',', ''); ?>€</p>
                    <p>Totale: <?php echo number_format(round(($sub_tot + $prezzo_spedizione) / 100.0, 2), 2, ',', ''); ?>€</p>
                    <form action="./ordina.php" method="post">
                        <label for="data_preferita_consegna">Data preferita consegna</label>
                        <input type="date" name="data_preferita_consegna" id="data_preferita_consegna" />
                        <br>
                        <label for="ind_consegna">Indirizzo di consegna*</label>
                        <input type="text" name="ind_consegna" id="ind_consegna" value="<?php echo htmlspecialchars(prop_utente('ind_residenza')); ?>" required />
                        <br>
                        <label for="metodo_pagamento">Metodo di pagamento</label>
                        <select name="metodo_pagamento" id="metodo_pagamento" required>
                            <option value="pag_cons">Pagamento alla consegna</option>
                            <option value="pp">PayPal</option>
                            <option value="altro">Altro</option>
                        </select>
                        <br>
                        <input type="hidden" name="rc" value="<?php echo $_SESSION['rand']; ?>" />
                        <input type="hidden" name="prodotti" value="<?php echo htmlspecialchars(json_encode($quant)); ?>" />
                        <input type="submit" name="ordina" value="Ordina" />
                    </form>
                    <a href="./index.php">Home</a>
            <?php endif;
            endif; 
        endif;
        close_conn();?>
    </body>
</html>

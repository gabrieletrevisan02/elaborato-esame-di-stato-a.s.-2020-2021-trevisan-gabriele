<?php

    session_start();
    if(isset($_SESSION['id'])){
       header('Location: index.php');
        exit;
    }

    $out = [];

    if (isset($_POST['mail']) && isset($_POST['password'])){
        require_once("connessioneDB.php");

        $mail = substr($_POST['mail'], 0, 320);
        $password = substr($_POST['password'], 0, 255);

        $stmt = NULL;
        if(
            ($stmt = $conn->prepare(
                "SELECT 
                    utente.id, utente.nome, utente.cognome,
                    utente.data_nascita, utente.ind_residenza, utente.mail, 
                    utente.permessi, dipendente_magazzino.id_magazzino, dipendente_magazzino_centrale.id_magazzino_centrale,
                    utente.password
                 FROM utente 
                 LEFT JOIN dipendente_magazzino ON(utente.id = dipendente_magazzino.id_utente) 
                 LEFT JOIN dipendente_magazzino_centrale ON(utente.id = dipendente_magazzino_centrale.id_utente) 
                 WHERE mail = ?"
            )) === FALSE ||
            $stmt->bind_param("s", $mail) === FALSE ||
            ($result = $stmt->execute()) === FALSE){
            $out[] = 'Errore generale!';
            close_conn_and_die();
        }

        $result = $stmt->get_result();
        
        if ($result->num_rows == 1){
            $result = $result->fetch_assoc();
            if(password_verify($password, $result['password'])){

                unset($result['password']);
                $_SESSION = $result;
                
                close_conn();
                header('Location: index.php');
                die();

            }else{
                $out[] = '<p class="error">Password sbagliata</p>';
            }
        }else{
            $out[] =  '<p class="error">Nome utente non trovato</p>';
        }
        close_conn();
    }

?>
<html>
    <head>
        <title>Accedi</title>
    </head>

    <body>
        <form method="post" action="" name="signin-form">
            <div class = "center">
            <div class="input">
                <label for="email">E-mail</label>
                <input type="text" name="mail" id="email" maxlength="320" required />
            </div>
            <br>
            <div class = "input">
                <label for="password">Password</label>
                <input type="password" name="password" maxlength="255" required />
            </div>
            <br>
            <button type="submit" name="login" value="login">Accedi</button>
            </div>
        </form>

        <a href="./">Indietro</a>
        <a href="./registrati.php">Registrati</a>

        <?php
        foreach($out as $str){
            echo $str;
        }
        ?>
    </body>
</html>
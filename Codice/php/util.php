<?php

    if(!isset($_SESSION['rand'])){
        $_SESSION['rand'] = rand();
    }
    function permessi_utente(){
        return $_SESSION['permessi'];
    }

    function ha_permesso(string $permesso){
        if(!isset($_SESSION['permessi'])){return false;}
        $permessi_utente = permessi_utente();
        if($permessi_utente === 'A'){return true;} //Admin può fare tutto
        return $permesso === $permessi_utente;
    }

    function utente_collegato(){
        return isset($_SESSION['id']);
    }

    function prop_utente(string $prop){ //TODO: filtrare
        return $_SESSION[strtolower($prop)];
    }

    function sql_out_of_range($errno){
        return $errno == 1264 || $errno == 1292 || $errno == 1690;
    }

?>
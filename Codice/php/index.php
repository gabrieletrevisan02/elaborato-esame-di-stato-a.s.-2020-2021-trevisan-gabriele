<?php
    session_start();

    require_once "util.php";
?>

<html>
    <head>
        <title>Home page</title>
    </head>
    <body>
        <?php if(utente_collegato()){echo '<p>Salve ' . prop_utente('nome') . '</p>';} ?>
        <div>
            <p>Azioni disponibili</p>
            <a href="./prodotti.php">Esplora prodotti</a><br>
            <a href="./punti_vendita.php">Visualizza punti vendita</a><br>
            <a href="./carrello.php">Visualizza carrello</a><br>
            <?php if(ha_permesso('A')): ?>
                <a href="admin.php">Accedi all'area amministrativa</a><br>
            <?php endif; ?>
            <?php if(ha_permesso('D') || ha_permesso('DC')): ?>
                <a href="dipendente.php">Accedi all'area dipendente</a><br>
            <?php endif; ?>
            <?php if(!utente_collegato()): ?>
                <a href="./login.php">Accedi</a>
            <?php else: ?>
                <a href="./logout.php">Esci</a>
            <?php endif; ?>
        </div>
    </body>
</html>

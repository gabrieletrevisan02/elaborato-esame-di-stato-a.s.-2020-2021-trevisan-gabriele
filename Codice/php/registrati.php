<?php

    session_start();
    if(isset($_SESSION['id'])){
        header('Location: index.php');
        die();
    }

    $out = [];

    if (
        isset($_POST['nome']) && 
        isset($_POST['cognome']) && 
        isset($_POST['mail']) && 
        isset($_POST['password']) && 
        isset($_POST['password2'])
    ){
        require_once("connessioneDB.php");
        $mail     = substr($_POST['mail'], 0, 320);
        $nome     = substr($_POST['nome'], 0, 32);
        $cognome  = substr($_POST['cognome'], 0, 32);
        $password = substr($_POST['password'], 0, 255);
        $password2 = substr($_POST['password2'], 0, 255);

        $data_nascita = "";
        if(
            isset($_POST['data_nascita']) && 
            strlen($_POST['data_nascita']) > 0 &&
            DateTime::createFromFormat('Y-m-d', $_POST['data_nascita']) !== FALSE
        ){
            $data_nascita = $_POST['data_nascita'];
        }

        $ind_residenza = "";
        if(
            isset($_POST['ind_residenza']) && 
            strlen($_POST['ind_residenza']) > 0
        ){
            $ind_residenza  = substr($_POST['ind_residenza'], 0, 64);
        }

        if(strcmp($password, $password2) !== 0){
           $out[] = '<p class="error">Le password non corrispondono!</p>';
        }else{
            $stmt = $result = NULL;
            if(
                ($stmt = $conn->prepare("SELECT id FROM utente WHERE mail = ?")) === FALSE ||
                $stmt->bind_param("s", $mail) === FALSE ||
                ($result = $stmt->execute()) === FALSE
            ){
                $out[] = 'Errore generale';
                close_conn_and_die();
            }

            $result = $stmt->get_result();

            if ($result->num_rows >= 1){
                $out[] = '<p class="error">La mail esiste gi&agrave;. Vuoi fare il <a href="./login.php">Login</a>?';
            }else{
                $stmt = $result = NULL;
                $password = password_hash($password, PASSWORD_DEFAULT);
                $stmt = $conn->prepare("INSERT INTO utente(nome, cognome, password, data_nascita, ind_residenza, mail) VALUES(?, ?, ?, ?, ?, ?)");
                if(
                    ($stmt === FALSE) ||
                    ($stmt->bind_param("ssssss", $nome, $cognome, $password, $data_nascita, $ind_residenza, $mail) === FALSE)
                ){
                    $out[] = 'Errore generale';
                    close_conn_and_die();
                }
                    
                if($result = $stmt->execute()){
                    $out[] = '<p>Registrazione effettuata</p>';
                    if (session_status() === PHP_SESSION_NONE) {
                        session_start();
                    }
                    $_SESSION = [
                        'id' => $conn->insert_id,
                        'nome' => $nome,
                        'cognome' => $cognome, 
                        'data_nascita' => $data_nascita, 
                        'ind_residenza' => $ind_residenza, 
                        'mail' => $mail,
                        'permessi' => 'U',
                        'id_magazzino' => null,
                        'id_magazzino_centrale' => null,
                    ];
                    header('location: index.php');
                    die();
                }else{
                    $out[] = 'Errore generale';
                    close_conn_and_die();
                }

            }
        }
        close_conn();
    }else{
        $out[] = '<p class="error">Per favore, compila tutti i campi richiesti</p>';
    }

?>
<html>
    <head>
        <title>Registrazione</title>
    </head>

    <body>
        <form method="post" action="" name="signin-form">
            <div class = "center">
                <div class="input">
                    <label for="nome">Nome*</label>
                    <input type="text" name="nome" id="nome" maxlength="32" value="<?php echo $_POST['nome'] ?? ''; ?>" required />
                </div>
                <br>
                <div class="input">
                    <label for="cognome">Cognome*</label>
                    <input type="text" name="cognome" id="cognome" maxlength="32" value="<?php echo $_POST['cognome'] ?? ''; ?>" required />
                </div>
                <br>
                <div class="input">
                    <label for="data_nascita">Data di nascita</label>
                    <input type="date" name="data_nascita" id="data_nascita" value="<?php echo $_POST['data_nascita'] ?? ''; ?>" />
                </div>
                <br>
                <div class="input">
                    <label for="ind_residenza">Indirizzo di residenza</label>
                    <input type="text" name="ind_residenza" id="ind_residenza" value="<?php echo $_POST['ind_residenza'] ?? ''; ?>" maxlength="64" />
                </div>
                <br>
                <div class="input">
                    <label for="mail">E-mail*</label>
                    <input type="text" name="mail" id="mail" maxlength="320" value="<?php echo $_POST['mail'] ?? ''; ?>" required />
                </div>
                <br>
                <div class = "input">
                    <label for="password">Password*</label>
                    <input type="password" name="password" id="password" maxlength="255" required />
                </div>
                <br>
                <div class = "input">
                    <label for="password2">Conferma Password*</label>
                    <input type="password" name="password2" id="password2" maxlength="255" required />
                </div>
                <br>
                <button type="submit" name="registrati">Registrati</button>
            </div>
        </form>
        <p class="hint">I campi contrassegnati con * sono obbligatori</p>
        <a href="./index.php">Indietro</a>

        <?php
            foreach($out as $str){
                echo $str;
            }
        ?>
    </body>
</html>
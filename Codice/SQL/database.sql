/*
	Tutti i prezzi sono in centesimi per evitare problemi di precisione dovuti all'errore della virgola mobile
		Uso int unsigned al posto di float
*/
create database if not exists gestione_supermercati;

use gestione_supermercati;

/*
	Taballa per tenere traccia delle
	informazioni degli utenti del servizio
*/
create table if not exists utente(
	id int auto_increment not null,
	nome varchar(32) not null,
	cognome varchar(32) not null,
	password varchar(255) not null,
	data_nascita date,
	ind_residenza varchar(64),
	/*
		Non sono matto, la scelta della lunghezza è stata fatta in base a questa risposta
		https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address#:~:text=%22There%20is%20a%20length%20limit,total%20length%20of%20320%20characters.
	*/
	mail varchar(320) unique not null,
	permessi enum('A', 'D', 'DC', 'U') not null default 'U',
	/*
		A: amministratore -Accesso completo
		D: dipendente -Può ordinare prodotti per un magazzino
		DC: dipendente magazzino centrale -Può gestire il magazzino centrale
		U: utente normale -Può ordinare la merce dal sito
	*/

	primary key(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	tabella per tenere traccia degli ordini
*/
create table if not exists ordine(
	id int auto_increment not null,
	id_utente int not null,
	data_ordine date not null,
	data_preferita_consegna date,
	ind_consegna varchar(64) not null,
	prezzo_spedizione int unsigned not null default 0,
	codice_tracciamento varchar(128),
	stato enum('att_pag', 'att_lav', 'in_lav', 'sped', 'cons', 'ann') not null,
	metodo_pagamento enum('pag_cons', 'pp', 'altro') not null,
	primary key(id),
	foreign key(id_utente) references utente(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	tabella contenente i dati dei fornitori
*/
create table if not exists fornitore(
	id int auto_increment not null,
	piva char(11) not null,
	nome varchar(64) not null,
	locazione varchar(64) not null,
	primary key(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	tabella contenente i reparti
*/
create table if not exists reparto(
	id int auto_increment not null,
	nome varchar(48) not null,
	primary key(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	tabella contenente i dati dei prodotti
*/
create table if not exists prodotto(
	codice_a_barre varchar(16) not null,
	id_fornitore int not null,
	/*
	Lunghezza codici a barre
	https://www.reminformatica.it/tipi-di-barcode/
	*/
	nome varchar(64) not null,
	descrizione varchar(1024) not null,
	peso mediumint unsigned, /*Espresso in grammi, peso massimo: 16777215 g --> 16777.215 kg*/
	iva enum('4', '5', '10', '22')  not null,
	prezzo_finale int unsigned not null,
	id_reparto int not null,
	prezzo_acquisto int unsigned not null,
	vendibile_online bit not null default 1,
	primary key(codice_a_barre),
	foreign key(id_fornitore) references fornitore(id),
	foreign key(id_reparto) references reparto(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	tabella contenente i dati dei magazzini centrlai
*/
create table if not exists magazzino_centrale(
	id int auto_increment not null,
	responsabile varchar(64) not null,
	posizione varchar(64) not null,
	primary key(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	tabella contenente i prodotti di un determinato ordine
*/
create table if not exists prodotto_ordine(
	id_ordine int not null,
	codice_prodotto varchar(16) not null,
	id_magazzino_centrale int not null,
	quant int unsigned not null default 1,
	primary key(id_ordine, codice_prodotto),
	foreign key(id_ordine) references ordine(id),
	foreign key(codice_prodotto) references prodotto(codice_a_barre),
	foreign key(id_magazzino_centrale) references magazzino_centrale(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;


/*
	tabella contenente i dati dei fornitori
*/
create table if not exists magazzino(
	id int auto_increment not null,
	responsabile varchar(64) not null,
	posizione varchar(64) not null,
	primary key(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	tabella che associa i dipendenti al magazzino in cui lavorano
*/
create table if not exists dipendente_magazzino(
	id_utente int not null,
	id_magazzino int not null,
	primary key(id_utente),
	foreign key(id_utente) references utente(id),
	foreign key(id_magazzino) references magazzino(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	tabella che associa i dipendenti al magazzino centrale in cui lavorano
*/
create table if not exists dipendente_magazzino_centrale(
	id_utente int not null,
	id_magazzino_centrale int not null,
	primary key(id_utente),
	foreign key(id_utente) references utente(id),
	foreign key(id_magazzino_centrale) references magazzino_centrale(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	tabella contenente i dati dei supermercati
*/
create table if not exists supermercato(
	id int auto_increment not null,
	nome varchar(64) not null,
	direttore varchar(64) not null,
	posizione varchar(64) not null,
	id_magazzino int not null,
	primary key(id),
	foreign key(id_magazzino) references magazzino(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	tabella in cui si tiene traccia della presenza dei prodotti nei magazzini
*/
create table if not exists prodotto_magazzino(
	codice_prodotto varchar(16) not null,
	id_magazzino int not null,
	sconto tinyint unsigned,
	quant int unsigned not null default 0,
	primary key(codice_prodotto, id_magazzino),
	foreign key(codice_prodotto) references prodotto(codice_a_barre),
	foreign key(id_magazzino) references magazzino(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	tabella in cui si tiene traccia della presenza dei prodotti nei magazzini centrali
*/
create table if not exists prodotto_magazzino_centrale(
	codice_prodotto varchar(16) not null,
	id_magazzino_centrale int not null,
	sconto tinyint unsigned, /*Solo per la vendita online ai privati*/
	quant int unsigned not null default 0,
	primary key(codice_prodotto, id_magazzino_centrale),
	foreign key(codice_prodotto) references prodotto(codice_a_barre),
	foreign key(id_magazzino_centrale) references magazzino_centrale(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	tabella che associa gli ordini ai magazzini centrlai
*/
create table if not exists ordine_a_magazzino_centrale(
	id int auto_increment not null,
	id_magazzino int not null,
	id_magazzino_centrale int not null,
	data_ordine date,
	completato bit default 0,
	primary key(id),
	foreign key(id_magazzino) references magazzino(id),
	foreign key(id_magazzino_centrale) references magazzino_centrale(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	tabella che collega i prodotti ordinati da un magazzino presso un magazzino centrale
*/
create table if not exists prodotto_ordine_a_magazzino_centrale(
	id_ordine_a_magazzino_centrale int not null,
	codice_prodotto varchar(16) not null,
	quant int unsigned not null default 1,
	primary key(id_ordine_a_magazzino_centrale, codice_prodotto),
	foreign key(id_ordine_a_magazzino_centrale) references ordine_a_magazzino_centrale(id),
	foreign key(codice_prodotto) references prodotto(codice_a_barre)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	tabella per tenere tracca dei prodotti in transito da magazzini verso altri magazzini
*/
create table if not exists prodotto_in_transito(
	id int auto_increment not null,
	codice_prodotto varchar(16) not null,
	quant int unsigned not null default 0,
	data_consegna date,
	stato enum('in_trans', 'cons', 'nd', 'perso') not null,

	primary key(id),
	foreign key(codice_prodotto) references prodotto(codice_a_barre)

)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	Da magazzino centrale a magazzino centrale
*/
create table if not exists prodotto_in_transito_mc_mc(
	prodotto_in_transito int not null,
	mc_part int not null,
	mc_dest int not null,
	
	primary key(prodotto_in_transito),
	foreign key(prodotto_in_transito) references prodotto_in_transito(id),
	foreign key(mc_part) references magazzino_centrale(id),
	foreign key(mc_dest) references magazzino_centrale(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	Da magazzino a magazzino
*/
create table if not exists prodotto_in_transito_m_m(
	prodotto_in_transito int not null,
	m_part int not null,
	m_dest int not null,
	
	primary key(prodotto_in_transito),
	foreign key(prodotto_in_transito) references prodotto_in_transito(id),
	foreign key(m_part) references magazzino(id),
	foreign key(m_dest) references magazzino(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	Da magazzino a magazzino centrale (resi, prodotti scaduti, ecc...)
*/
create table if not exists prodotto_in_transito_m_mc(
	prodotto_in_transito int not null,
	m_part int not null,
	mc_dest int not null,
	
	primary key(prodotto_in_transito),
	foreign key(prodotto_in_transito) references prodotto_in_transito(id),
	foreign key(m_part) references magazzino(id),
	foreign key(mc_dest) references magazzino_centrale(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

/*
	Da magazzino centrale a magazzino
*/
create table if not exists prodotto_in_transito_mc_m(
	prodotto_in_transito int not null,
	mc_part int not null,
	m_dest int not null,
	
	primary key(prodotto_in_transito),
	foreign key(prodotto_in_transito) references prodotto_in_transito(id),
	foreign key(mc_part) references magazzino_centrale(id),
	foreign key(m_dest) references magazzino(id)
)Engine=InnoDB char set utf8mb4 collate utf8mb4_unicode_ci;

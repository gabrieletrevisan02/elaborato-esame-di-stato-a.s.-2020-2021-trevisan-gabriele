/*Prodotti vendibili online (Solo quelli presenti nei magazzini centarli e segnati per la vendita online)*/
    select prodotto.codice_a_barre, prodotto.nome, prodotto.descrizione, prodotto.prezzo_finale, prodotto.iva, reparto.nome, fornitore.nome, prodotto_magazzino_centrale.id_magazzino_centrale, magazzino_centrale.posizione, prodotto_magazzino_centrale.quant as 'disponibilita'
    from prodotto
    inner join prodotto_magazzino_centrale on(prodotto.codice_a_barre = prodotto_magazzino_centrale.codice_prodotto and prodotto.vendibile_online = 1)
    inner join magazzino_centrale on(prodotto_magazzino_centrale.id_magazzino_centrale = magazzino_centrale.id)
    inner join reparto on(prodotto.id_reparto = reparto.id)
    inner join fornitore on(prodotto.id_fornitore = fornitore.id);
/* */

/*Totale prodotti in tutti i magazzini (centrali e non) e in transito (Non consegnati, non persi e non con stato "Non definito")*/
    select prodotto.codice_a_barre, prodotto.nome, prodotto.prezzo_finale, prodotto.prezzo_finale * t.tot_prod as 'valore_totale'
    from (
        select p.codice_a_barre, 
            coalesce((
                select sum(coalesce(prodotto_magazzino_centrale.quant, 0))
                from prodotto_magazzino_centrale 
                where prodotto_magazzino_centrale.codice_prodotto = p.codice_a_barre
                group by p.codice_a_barre
            ), 0) +
            coalesce((
                select sum(coalesce(prodotto_magazzino.quant, 0))
                from prodotto_magazzino 
                where prodotto_magazzino.codice_prodotto = p.codice_a_barre 
                group by p.codice_a_barre
        ), 0) +
        coalesce((
                select sum(coalesce(prodotto_in_transito.quant, 0))
                from prodotto_in_transito 
                where prodotto_in_transito.codice_prodotto = p.codice_a_barre and
                prodotto_in_transito.stato = 'in_trans'
                group by p.codice_a_barre
        ), 0) as 'tot_prod'
        from prodotto p
    ) as t
    inner join prodotto on(t.codice_a_barre = prodotto.codice_a_barre);
/* */

/*Dati di tutti gli ordini*/
    select utente.nome, utente.cognome, utente.mail, ordine.ind_consegna, fornitore.nome, prodotto.nome, prodotto.codice_a_barre, prodotto.prezzo_finale, prodotto.iva, prodotto_ordine.quant, prodotto.peso, ordine.prezzo_spedizione,
        prodotto.prezzo_finale * prodotto_ordine.quant as 'subtotale', prodotto.prezzo_finale * prodotto_ordine.quant + ordine.prezzo_spedizione as 'totale'
    from prodotto
    inner join prodotto_ordine on(prodotto_ordine.codice_prodotto = prodotto.codice_a_barre)
    inner join ordine on(prodotto_ordine.id_ordine = ordine.id)
    inner join utente on(ordine.id_utente = utente.id)
    inner join fornitore on(prodotto.id_fornitore = fornitore.id);
/* */

/*Totali di tutti gli ordini*/
select sum(prodotto.prezzo_finale * prodotto_ordine.quant + ordine.prezzo_spedizione) as 'totale_ordine'
    from prodotto
    inner join prodotto_ordine on(prodotto_ordine.codice_prodotto = prodotto.codice_a_barre)
    inner join ordine on(prodotto_ordine.id_ordine = ordine.id and ordine.stato <> 'ann')
/* */

/*Totali di tutti gli ordini, con spedizione a peso (ordine.prezzo_spedizione al chilo)*/
    select 
        sum(prodotto.prezzo_finale * prodotto_ordine.quant) as 'subtotale', 
        sum(prodotto.peso * prodotto_ordine.quant) as 'peso_totale',  
        sum(prodotto.peso * prodotto_ordine.quant) / 1000 * ordine.prezzo_spedizione as 'prezzo_spedizione',
        sum(prodotto.prezzo_finale * prodotto_ordine.quant) + sum(prodotto.peso * prodotto_ordine.quant) / 1000 * ordine.prezzo_spedizione as 'totale'
    from prodotto
    inner join prodotto_ordine on(prodotto_ordine.codice_prodotto = prodotto.codice_a_barre)
    inner join ordine on(prodotto_ordine.id_ordine = ordine.id);
/* */

/*Lista di tutti i prodotti in transito*/
    select prodotto.codice_a_barre, prodotto.nome, prodotto.prezzo_finale, prodotto.peso, prodotto_in_transito.quant
    from prodotto
    inner join prodotto_in_transito on(prodotto_in_transito.codice_prodotto = prodotto.codice_a_barre and prodotto_in_transito.stato = 'in_trans');
/* */

/*Lista di tutti i prodotti in transito, con relativa sede di partenza e destinazione*/
    select prodotto.codice_a_barre, prodotto.nome, prodotto_in_transito.quant,
        coalesce(prodotto_in_transito_m_mc.m_part, prodotto_in_transito_m_m.m_part, '-------') as 'm_part',
        coalesce(prodotto_in_transito_mc_mc.mc_part, prodotto_in_transito_mc_m.mc_part, '-------') as 'mc_part',
        coalesce(prodotto_in_transito_m_m.m_dest, prodotto_in_transito_mc_m.m_dest, '-------') as 'm_dest',
        coalesce(prodotto_in_transito_mc_mc.mc_dest, prodotto_in_transito_m_mc.mc_dest, '-------') as 'mc_dest',
        if(prodotto_in_transito_m_mc.m_part is not null or prodotto_in_transito_m_m.m_part is not null, m.posizione, '-------') as 'm_pos_part',
        if(prodotto_in_transito_mc_mc.mc_part is not null or prodotto_in_transito_mc_m.mc_part is not null, mc.posizione, '-------') as 'mc_pos_part',
        if(prodotto_in_transito_m_m.m_dest is not null or prodotto_in_transito_mc_m.m_dest is not null, m.posizione, '-------') as 'm_pos_dest',
        if(prodotto_in_transito_mc_mc.mc_dest is not null or prodotto_in_transito_m_mc.mc_dest is not null, mc.posizione, '-------') as 'mc_pos_dest'
    from prodotto
    inner join prodotto_in_transito  on(prodotto.codice_a_barre = prodotto_in_transito.codice_prodotto)
    left join prodotto_in_transito_m_mc  on(prodotto_in_transito.id = prodotto_in_transito_m_mc.prodotto_in_transito)
    left join prodotto_in_transito_m_m  on(prodotto_in_transito.id = prodotto_in_transito_m_m.prodotto_in_transito)
    left join prodotto_in_transito_mc_mc  on(prodotto_in_transito.id = prodotto_in_transito_mc_mc.prodotto_in_transito)
    left join prodotto_in_transito_mc_m  on(prodotto_in_transito.id = prodotto_in_transito_mc_m.prodotto_in_transito)
    left join magazzino_centrale as mc on(prodotto_in_transito_m_mc.mc_dest = mc.id or prodotto_in_transito_mc_mc.mc_part = mc.id or prodotto_in_transito_mc_mc.mc_dest = mc.id or prodotto_in_transito_mc_m.mc_part = mc.id)
    left join magazzino as m on(prodotto_in_transito_m_mc.m_part = m.id or prodotto_in_transito_m_m.m_part = m.id or prodotto_in_transito_m_m.m_dest = m.id or prodotto_in_transito_mc_m.m_dest = m.id);
/* */

/*Totale valore dei prodotti in transito*/
    select sum(prodotto.prezzo_finale * prodotto_in_transito.quant) as 'totale_valore_in_transito'
    from prodotto
    inner join prodotto_in_transito on(prodotto_in_transito.codice_prodotto = prodotto.codice_a_barre and prodotto_in_transito.stato = 'in_trans');
/* */
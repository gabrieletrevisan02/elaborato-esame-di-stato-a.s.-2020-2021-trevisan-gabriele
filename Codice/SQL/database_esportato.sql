-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Mag 31, 2021 alle 15:24
-- Versione del server: 10.4.14-MariaDB
-- Versione PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gestione_supermercati`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `dipendente_magazzino`
--

CREATE TABLE `dipendente_magazzino` (
  `id_utente` int(11) NOT NULL,
  `id_magazzino` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `dipendente_magazzino`
--

INSERT INTO `dipendente_magazzino` (`id_utente`, `id_magazzino`) VALUES
(1, 2),
(6, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `dipendente_magazzino_centrale`
--

CREATE TABLE `dipendente_magazzino_centrale` (
  `id_utente` int(11) NOT NULL,
  `id_magazzino_centrale` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `dipendente_magazzino_centrale`
--

INSERT INTO `dipendente_magazzino_centrale` (`id_utente`, `id_magazzino_centrale`) VALUES
(4, 2),
(8, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `fornitore`
--

CREATE TABLE `fornitore` (
  `id` int(11) NOT NULL,
  `piva` char(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nome` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locazione` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `fornitore`
--

INSERT INTO `fornitore` (`id`, `piva`, `nome`, `locazione`) VALUES
(1, '12345678910', 'Buffetti', 'Padova'),
(2, '12345678911', 'Fornitore', 'Milano');

-- --------------------------------------------------------

--
-- Struttura della tabella `magazzino`
--

CREATE TABLE `magazzino` (
  `id` int(11) NOT NULL,
  `responsabile` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `posizione` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `magazzino`
--

INSERT INTO `magazzino` (`id`, `responsabile`, `posizione`) VALUES
(2, 'Pippo', 'Padova'),
(3, 'Pluto', 'Genova'),
(4, 'Aldo Giovanni', 'Rovigo');

-- --------------------------------------------------------

--
-- Struttura della tabella `magazzino_centrale`
--

CREATE TABLE `magazzino_centrale` (
  `id` int(11) NOT NULL,
  `responsabile` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `posizione` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `magazzino_centrale`
--

INSERT INTO `magazzino_centrale` (`id`, `responsabile`, `posizione`) VALUES
(2, 'Minnie', 'Padova'),
(3, 'Topolino', 'Milano'),
(4, 'Giacomo Giacomini', 'Venezia');

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine`
--

CREATE TABLE `ordine` (
  `id` int(11) NOT NULL,
  `id_utente` int(11) NOT NULL,
  `data_ordine` date NOT NULL,
  `data_preferita_consegna` date DEFAULT NULL,
  `ind_consegna` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prezzo_spedizione` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `codice_tracciamento` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stato` enum('att_pag','att_lav','in_lav','sped','cons','ann') COLLATE utf8mb4_unicode_ci NOT NULL,
  `metodo_pagamento` enum('pag_cons','pp','altro') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `ordine`
--

INSERT INTO `ordine` (`id`, `id_utente`, `data_ordine`, `data_preferita_consegna`, `ind_consegna`, `prezzo_spedizione`, `codice_tracciamento`, `stato`, `metodo_pagamento`) VALUES
(28, 1, '2021-05-27', '0000-00-00', 'Via S. Pertini 7', 100, NULL, 'att_lav', 'pag_cons'),
(29, 4, '2021-05-29', '0000-00-00', 'via S. Pertini 7', 100, NULL, 'att_lav', 'pag_cons');

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine_a_magazzino_centrale`
--

CREATE TABLE `ordine_a_magazzino_centrale` (
  `id` int(11) NOT NULL,
  `id_magazzino` int(11) NOT NULL,
  `id_magazzino_centrale` int(11) NOT NULL,
  `data_ordine` date DEFAULT NULL,
  `completato` bit(1) DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `ordine_a_magazzino_centrale`
--

INSERT INTO `ordine_a_magazzino_centrale` (`id`, `id_magazzino`, `id_magazzino_centrale`, `data_ordine`, `completato`) VALUES
(28, 2, 2, '2021-05-23', b'1'),
(29, 2, 2, '2021-05-23', b'1'),
(30, 2, 3, '2021-05-23', b'1');

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto`
--

CREATE TABLE `prodotto` (
  `codice_a_barre` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_fornitore` int(11) NOT NULL,
  `nome` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descrizione` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iva` enum('4','5','10','22') COLLATE utf8mb4_unicode_ci NOT NULL,
  `prezzo_finale` int(10) UNSIGNED NOT NULL,
  `id_reparto` int(11) NOT NULL,
  `prezzo_acquisto` int(10) UNSIGNED NOT NULL,
  `vendibile_online` bit(1) NOT NULL DEFAULT b'1',
  `peso` mediumint(8) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `prodotto`
--

INSERT INTO `prodotto` (`codice_a_barre`, `id_fornitore`, `nome`, `descrizione`, `iva`, `prezzo_finale`, `id_reparto`, `prezzo_acquisto`, `vendibile_online`, `peso`) VALUES
('1234567891011123', 1, 'Lettore Smart Card', 'Lettore Smart Card USB 2.0', '22', 1890, 2, 1000, b'1', 120),
('1234567891011128', 2, 'Formaggio in fette', 'Formaggio in fette', '22', 1500, 1, 1100, b'0', 250),
('1234567891011180', 2, 'Prosciutto Crudo', 'Prosciutto Crudo in fette', '22', 950, 1, 550, b'1', 150),
('1234567891011187', 2, 'Prosciutto Cotto', 'Prosciutto Cotto in fette', '22', 850, 1, 550, b'1', 200),
('123456789101177', 1, 'Gomma', 'Gomma', '22', 50, 2, 10, b'1', 20),
('123456789101188', 1, 'Pacco 10 penne BLU', 'Pacco di 10 penne BLU a sfera', '22', 250, 2, 150, b'1', 20);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_in_transito`
--

CREATE TABLE `prodotto_in_transito` (
  `id` int(11) NOT NULL,
  `codice_prodotto` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quant` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `data_consegna` date DEFAULT NULL,
  `stato` enum('in_trans','cons','nd','perso') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `prodotto_in_transito`
--

INSERT INTO `prodotto_in_transito` (`id`, `codice_prodotto`, `quant`, `data_consegna`, `stato`) VALUES
(1, '1234567891011123', 10, '2021-05-27', 'in_trans'),
(16, '123456789101177', 5, '0000-00-00', 'in_trans'),
(20, '123456789101177', 10, '2021-05-28', 'in_trans'),
(21, '123456789101177', 10, '2021-05-28', 'in_trans'),
(22, '123456789101177', 5, '0000-00-00', 'in_trans'),
(23, '123456789101177', 2, '2021-05-23', 'in_trans');

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_in_transito_mc_m`
--

CREATE TABLE `prodotto_in_transito_mc_m` (
  `prodotto_in_transito` int(11) NOT NULL,
  `mc_part` int(11) NOT NULL,
  `m_dest` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `prodotto_in_transito_mc_m`
--

INSERT INTO `prodotto_in_transito_mc_m` (`prodotto_in_transito`, `mc_part`, `m_dest`) VALUES
(1, 2, 2),
(16, 3, 2),
(20, 3, 2),
(21, 3, 2),
(22, 3, 3),
(23, 3, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_in_transito_mc_mc`
--

CREATE TABLE `prodotto_in_transito_mc_mc` (
  `prodotto_in_transito` int(11) NOT NULL,
  `mc_part` int(11) NOT NULL,
  `mc_dest` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_in_transito_m_m`
--

CREATE TABLE `prodotto_in_transito_m_m` (
  `prodotto_in_transito` int(11) NOT NULL,
  `m_part` int(11) NOT NULL,
  `m_dest` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_in_transito_m_mc`
--

CREATE TABLE `prodotto_in_transito_m_mc` (
  `prodotto_in_transito` int(11) NOT NULL,
  `m_part` int(11) NOT NULL,
  `mc_dest` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_magazzino`
--

CREATE TABLE `prodotto_magazzino` (
  `codice_prodotto` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_magazzino` int(11) NOT NULL,
  `sconto` tinyint(3) UNSIGNED DEFAULT NULL,
  `quant` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `prodotto_magazzino`
--

INSERT INTO `prodotto_magazzino` (`codice_prodotto`, `id_magazzino`, `sconto`, `quant`) VALUES
('1234567891011123', 2, 0, 20),
('1234567891011123', 3, 0, 12),
('1234567891011187', 2, 0, 3),
('123456789101177', 2, 0, 54),
('123456789101188', 2, 0, 19);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_magazzino_centrale`
--

CREATE TABLE `prodotto_magazzino_centrale` (
  `codice_prodotto` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_magazzino_centrale` int(11) NOT NULL,
  `quant` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `sconto` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `prodotto_magazzino_centrale`
--

INSERT INTO `prodotto_magazzino_centrale` (`codice_prodotto`, `id_magazzino_centrale`, `quant`, `sconto`) VALUES
('1234567891011123', 2, 0, NULL),
('1234567891011123', 3, 48, NULL),
('1234567891011123', 4, 18, 15),
('1234567891011128', 2, 11, NULL),
('1234567891011128', 3, 98, NULL),
('1234567891011187', 2, 14, NULL),
('123456789101177', 3, 31, NULL),
('123456789101188', 2, 188, 10),
('123456789101188', 4, 150, 5);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_ordine`
--

CREATE TABLE `prodotto_ordine` (
  `id_ordine` int(11) NOT NULL,
  `codice_prodotto` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_magazzino_centrale` int(11) NOT NULL,
  `quant` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `prodotto_ordine`
--

INSERT INTO `prodotto_ordine` (`id_ordine`, `codice_prodotto`, `id_magazzino_centrale`, `quant`) VALUES
(28, '1234567891011123', 3, 2),
(29, '1234567891011123', 3, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_ordine_a_magazzino_centrale`
--

CREATE TABLE `prodotto_ordine_a_magazzino_centrale` (
  `id_ordine_a_magazzino_centrale` int(11) NOT NULL,
  `codice_prodotto` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quant` int(11) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `prodotto_ordine_a_magazzino_centrale`
--

INSERT INTO `prodotto_ordine_a_magazzino_centrale` (`id_ordine_a_magazzino_centrale`, `codice_prodotto`, `quant`) VALUES
(28, '1234567891011123', 13),
(29, '123456789101188', 12),
(30, '123456789101188', 7);

-- --------------------------------------------------------

--
-- Struttura della tabella `reparto`
--

CREATE TABLE `reparto` (
  `id` int(11) NOT NULL,
  `nome` varchar(48) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `reparto`
--

INSERT INTO `reparto` (`id`, `nome`) VALUES
(1, 'Alimentari'),
(2, 'Cancelleria');

-- --------------------------------------------------------

--
-- Struttura della tabella `supermercato`
--

CREATE TABLE `supermercato` (
  `id` int(11) NOT NULL,
  `nome` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direttore` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `posizione` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_magazzino` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `supermercato`
--

INSERT INTO `supermercato` (`id`, `nome`, `direttore`, `posizione`, `id_magazzino`) VALUES
(1, 'Supermercato 1', 'Gianpaolino', 'Padova', 2),
(2, 'Supermercato 2', 'Libretto', 'Genova', 3),
(3, 'Supermercato 3', 'De Ciro', 'Venezia', 4),
(4, 'Supermercato 4', 'Pino Pinuccio', 'Como', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `id` int(11) NOT NULL,
  `nome` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cognome` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_nascita` date DEFAULT NULL,
  `ind_residenza` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail` varchar(320) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permessi` enum('A','D','DC','U') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'U'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`id`, `nome`, `cognome`, `password`, `data_nascita`, `ind_residenza`, `mail`, `permessi`) VALUES
(1, 'utente1', 'utente', '$2y$10$u2vPU6kpHrp6Figz.TnoAuXUsV6OE/prwH9wxwe6DZyIcRw21s6eO', '1980-05-12', 'Via S. Pertini 7', 'utente1@example.com', 'D'),
(3, 'Gabriele', 'Trevisan', '$2y$10$aYUp/noDtwBhbHbwitTOTelOOQyjXxVp9NsrCaBtW9isGK1h.q/nC', '0000-00-00', '', 'gabt@gmail.com', 'A'),
(4, 'Gabriele', 'Trevisan', '$2y$10$3nYzfhPlp.w47852Qbaz1esq3PFg2HdjeIfosq7IGOyQY7ExeP7wm', '2002-07-02', 'via S. Pertini 7', 'gabrieletrevisan02@gmail.com', 'A'),
(5, 'Gabriele', 'Trevisan', '$2y$10$UIbISjPA6kXjPkDcAUEfH.RMGMx7vHNLpsTGo40m/gpdwuqQ1OT0O', '0000-00-00', 'via S. Pertini 7', 'gabrieletrevisan02@gm2ail.com', 'U'),
(6, 'Gabriele2', 'Trevisan2', '$2y$10$LdkLkvUp.fMN5l80Fcf5/.LyT2GN1992M.Tzct.HbXdDLm2PsiAVm', '0000-00-00', 'via S. Pertini 7', 'gt02@gmail.com', 'D'),
(7, 'Gabriele', 'Trevisan', '$2y$10$KNzpWSh.w6PwpVCDtw.eEuT9eD9KzeoA.xF.zf.fs6TK/1E.bEv5q', '0000-00-00', 'via S. Pertini 7', 'wfdasf', 'U'),
(8, '1234', '5678', '$2y$10$8LY3gf3dIMhDy5ZUgPnGZ.bB6/XSCXmNZlCFXTnuLVuggvDQwVnEC', '2002-07-02', 'via S. Pertini 7', 'test@gmail.com', 'DC');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `dipendente_magazzino`
--
ALTER TABLE `dipendente_magazzino`
  ADD PRIMARY KEY (`id_utente`),
  ADD KEY `id_magazzino` (`id_magazzino`);

--
-- Indici per le tabelle `dipendente_magazzino_centrale`
--
ALTER TABLE `dipendente_magazzino_centrale`
  ADD PRIMARY KEY (`id_utente`),
  ADD KEY `id_magazzino_centrale` (`id_magazzino_centrale`);

--
-- Indici per le tabelle `fornitore`
--
ALTER TABLE `fornitore`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `magazzino`
--
ALTER TABLE `magazzino`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `magazzino_centrale`
--
ALTER TABLE `magazzino_centrale`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ordine`
--
ALTER TABLE `ordine`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_utente` (`id_utente`);

--
-- Indici per le tabelle `ordine_a_magazzino_centrale`
--
ALTER TABLE `ordine_a_magazzino_centrale`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_magazzino` (`id_magazzino`),
  ADD KEY `id_magazzino_centrale` (`id_magazzino_centrale`);

--
-- Indici per le tabelle `prodotto`
--
ALTER TABLE `prodotto`
  ADD PRIMARY KEY (`codice_a_barre`),
  ADD KEY `id_fornitore` (`id_fornitore`),
  ADD KEY `id_reparto` (`id_reparto`);

--
-- Indici per le tabelle `prodotto_in_transito`
--
ALTER TABLE `prodotto_in_transito`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codice_prodotto` (`codice_prodotto`);

--
-- Indici per le tabelle `prodotto_in_transito_mc_m`
--
ALTER TABLE `prodotto_in_transito_mc_m`
  ADD PRIMARY KEY (`prodotto_in_transito`),
  ADD KEY `mc_part` (`mc_part`),
  ADD KEY `m_dest` (`m_dest`);

--
-- Indici per le tabelle `prodotto_in_transito_mc_mc`
--
ALTER TABLE `prodotto_in_transito_mc_mc`
  ADD PRIMARY KEY (`prodotto_in_transito`),
  ADD KEY `mc_part` (`mc_part`),
  ADD KEY `mc_dest` (`mc_dest`);

--
-- Indici per le tabelle `prodotto_in_transito_m_m`
--
ALTER TABLE `prodotto_in_transito_m_m`
  ADD PRIMARY KEY (`prodotto_in_transito`),
  ADD KEY `m_part` (`m_part`),
  ADD KEY `m_dest` (`m_dest`);

--
-- Indici per le tabelle `prodotto_in_transito_m_mc`
--
ALTER TABLE `prodotto_in_transito_m_mc`
  ADD PRIMARY KEY (`prodotto_in_transito`),
  ADD KEY `m_part` (`m_part`),
  ADD KEY `mc_dest` (`mc_dest`);

--
-- Indici per le tabelle `prodotto_magazzino`
--
ALTER TABLE `prodotto_magazzino`
  ADD PRIMARY KEY (`codice_prodotto`,`id_magazzino`),
  ADD KEY `id_magazzino` (`id_magazzino`);

--
-- Indici per le tabelle `prodotto_magazzino_centrale`
--
ALTER TABLE `prodotto_magazzino_centrale`
  ADD PRIMARY KEY (`codice_prodotto`,`id_magazzino_centrale`),
  ADD KEY `id_magazzino_centrale` (`id_magazzino_centrale`);

--
-- Indici per le tabelle `prodotto_ordine`
--
ALTER TABLE `prodotto_ordine`
  ADD PRIMARY KEY (`id_ordine`,`codice_prodotto`,`id_magazzino_centrale`),
  ADD KEY `codice_prodotto` (`codice_prodotto`),
  ADD KEY `id_magazzino_centrale` (`id_magazzino_centrale`);

--
-- Indici per le tabelle `prodotto_ordine_a_magazzino_centrale`
--
ALTER TABLE `prodotto_ordine_a_magazzino_centrale`
  ADD PRIMARY KEY (`id_ordine_a_magazzino_centrale`,`codice_prodotto`),
  ADD KEY `codice_prodotto` (`codice_prodotto`);

--
-- Indici per le tabelle `reparto`
--
ALTER TABLE `reparto`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `supermercato`
--
ALTER TABLE `supermercato`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_magazzino` (`id_magazzino`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mail` (`mail`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `fornitore`
--
ALTER TABLE `fornitore`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `magazzino`
--
ALTER TABLE `magazzino`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `magazzino_centrale`
--
ALTER TABLE `magazzino_centrale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `ordine`
--
ALTER TABLE `ordine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT per la tabella `ordine_a_magazzino_centrale`
--
ALTER TABLE `ordine_a_magazzino_centrale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT per la tabella `prodotto_in_transito`
--
ALTER TABLE `prodotto_in_transito`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT per la tabella `reparto`
--
ALTER TABLE `reparto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `supermercato`
--
ALTER TABLE `supermercato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `utente`
--
ALTER TABLE `utente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `dipendente_magazzino`
--
ALTER TABLE `dipendente_magazzino`
  ADD CONSTRAINT `dipendente_magazzino_ibfk_1` FOREIGN KEY (`id_utente`) REFERENCES `utente` (`id`),
  ADD CONSTRAINT `dipendente_magazzino_ibfk_2` FOREIGN KEY (`id_magazzino`) REFERENCES `magazzino` (`id`);

--
-- Limiti per la tabella `dipendente_magazzino_centrale`
--
ALTER TABLE `dipendente_magazzino_centrale`
  ADD CONSTRAINT `dipendente_magazzino_centrale_ibfk_1` FOREIGN KEY (`id_utente`) REFERENCES `utente` (`id`),
  ADD CONSTRAINT `dipendente_magazzino_centrale_ibfk_2` FOREIGN KEY (`id_magazzino_centrale`) REFERENCES `magazzino_centrale` (`id`);

--
-- Limiti per la tabella `ordine`
--
ALTER TABLE `ordine`
  ADD CONSTRAINT `ordine_ibfk_1` FOREIGN KEY (`id_utente`) REFERENCES `utente` (`id`);

--
-- Limiti per la tabella `ordine_a_magazzino_centrale`
--
ALTER TABLE `ordine_a_magazzino_centrale`
  ADD CONSTRAINT `ordine_a_magazzino_centrale_ibfk_1` FOREIGN KEY (`id_magazzino`) REFERENCES `magazzino` (`id`),
  ADD CONSTRAINT `ordine_a_magazzino_centrale_ibfk_2` FOREIGN KEY (`id_magazzino_centrale`) REFERENCES `magazzino_centrale` (`id`);

--
-- Limiti per la tabella `prodotto`
--
ALTER TABLE `prodotto`
  ADD CONSTRAINT `prodotto_ibfk_1` FOREIGN KEY (`id_fornitore`) REFERENCES `fornitore` (`id`),
  ADD CONSTRAINT `prodotto_ibfk_2` FOREIGN KEY (`id_reparto`) REFERENCES `reparto` (`id`);

--
-- Limiti per la tabella `prodotto_in_transito`
--
ALTER TABLE `prodotto_in_transito`
  ADD CONSTRAINT `prodotto_in_transito_ibfk_1` FOREIGN KEY (`codice_prodotto`) REFERENCES `prodotto` (`codice_a_barre`);

--
-- Limiti per la tabella `prodotto_in_transito_mc_m`
--
ALTER TABLE `prodotto_in_transito_mc_m`
  ADD CONSTRAINT `prodotto_in_transito_mc_m_ibfk_1` FOREIGN KEY (`prodotto_in_transito`) REFERENCES `prodotto_in_transito` (`id`),
  ADD CONSTRAINT `prodotto_in_transito_mc_m_ibfk_2` FOREIGN KEY (`mc_part`) REFERENCES `magazzino_centrale` (`id`),
  ADD CONSTRAINT `prodotto_in_transito_mc_m_ibfk_3` FOREIGN KEY (`m_dest`) REFERENCES `magazzino` (`id`);

--
-- Limiti per la tabella `prodotto_in_transito_mc_mc`
--
ALTER TABLE `prodotto_in_transito_mc_mc`
  ADD CONSTRAINT `prodotto_in_transito_mc_mc_ibfk_1` FOREIGN KEY (`prodotto_in_transito`) REFERENCES `prodotto_in_transito` (`id`),
  ADD CONSTRAINT `prodotto_in_transito_mc_mc_ibfk_2` FOREIGN KEY (`mc_part`) REFERENCES `magazzino_centrale` (`id`),
  ADD CONSTRAINT `prodotto_in_transito_mc_mc_ibfk_3` FOREIGN KEY (`mc_dest`) REFERENCES `magazzino_centrale` (`id`);

--
-- Limiti per la tabella `prodotto_in_transito_m_m`
--
ALTER TABLE `prodotto_in_transito_m_m`
  ADD CONSTRAINT `prodotto_in_transito_m_m_ibfk_1` FOREIGN KEY (`prodotto_in_transito`) REFERENCES `prodotto_in_transito` (`id`),
  ADD CONSTRAINT `prodotto_in_transito_m_m_ibfk_2` FOREIGN KEY (`m_part`) REFERENCES `magazzino` (`id`),
  ADD CONSTRAINT `prodotto_in_transito_m_m_ibfk_3` FOREIGN KEY (`m_dest`) REFERENCES `magazzino` (`id`);

--
-- Limiti per la tabella `prodotto_in_transito_m_mc`
--
ALTER TABLE `prodotto_in_transito_m_mc`
  ADD CONSTRAINT `prodotto_in_transito_m_mc_ibfk_1` FOREIGN KEY (`prodotto_in_transito`) REFERENCES `prodotto_in_transito` (`id`),
  ADD CONSTRAINT `prodotto_in_transito_m_mc_ibfk_2` FOREIGN KEY (`m_part`) REFERENCES `magazzino` (`id`),
  ADD CONSTRAINT `prodotto_in_transito_m_mc_ibfk_3` FOREIGN KEY (`mc_dest`) REFERENCES `magazzino_centrale` (`id`);

--
-- Limiti per la tabella `prodotto_magazzino`
--
ALTER TABLE `prodotto_magazzino`
  ADD CONSTRAINT `prodotto_magazzino_ibfk_1` FOREIGN KEY (`codice_prodotto`) REFERENCES `prodotto` (`codice_a_barre`),
  ADD CONSTRAINT `prodotto_magazzino_ibfk_2` FOREIGN KEY (`id_magazzino`) REFERENCES `magazzino` (`id`);

--
-- Limiti per la tabella `prodotto_magazzino_centrale`
--
ALTER TABLE `prodotto_magazzino_centrale`
  ADD CONSTRAINT `prodotto_magazzino_centrale_ibfk_1` FOREIGN KEY (`codice_prodotto`) REFERENCES `prodotto` (`codice_a_barre`),
  ADD CONSTRAINT `prodotto_magazzino_centrale_ibfk_2` FOREIGN KEY (`id_magazzino_centrale`) REFERENCES `magazzino_centrale` (`id`);

--
-- Limiti per la tabella `prodotto_ordine`
--
ALTER TABLE `prodotto_ordine`
  ADD CONSTRAINT `prodotto_ordine_ibfk_1` FOREIGN KEY (`id_ordine`) REFERENCES `ordine` (`id`),
  ADD CONSTRAINT `prodotto_ordine_ibfk_2` FOREIGN KEY (`codice_prodotto`) REFERENCES `prodotto` (`codice_a_barre`),
  ADD CONSTRAINT `prodotto_ordine_ibfk_3` FOREIGN KEY (`id_magazzino_centrale`) REFERENCES `magazzino_centrale` (`id`);

--
-- Limiti per la tabella `prodotto_ordine_a_magazzino_centrale`
--
ALTER TABLE `prodotto_ordine_a_magazzino_centrale`
  ADD CONSTRAINT `prodotto_ordine_a_magazzino_centrale_ibfk_1` FOREIGN KEY (`id_ordine_a_magazzino_centrale`) REFERENCES `ordine_a_magazzino_centrale` (`id`),
  ADD CONSTRAINT `prodotto_ordine_a_magazzino_centrale_ibfk_2` FOREIGN KEY (`codice_prodotto`) REFERENCES `prodotto` (`codice_a_barre`);

--
-- Limiti per la tabella `supermercato`
--
ALTER TABLE `supermercato`
  ADD CONSTRAINT `supermercato_ibfk_1` FOREIGN KEY (`id_magazzino`) REFERENCES `magazzino` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

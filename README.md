<h1>Elaborato Esame di stato di Trevisan Gabriele</h1>
Per utilizzare il codice PHP &egrave; necessario creare il database importando il file 
<pre>Codice/SQL/database.sql</pre>
<b>Se si vuole avere già dei valori con cui lavorare</b>, utilizzare invece il file
<pre>Codice/SQL/database_esportato.sql</pre>
Per il funzionamento del codice php, &egrave; necessario creare nella stessa directory di index.php il seguente file
<pre>Codice/php/credenziali_db.php</pre>
e inserire il codice
<pre>
&lt;?php
    $servername = 'server_database';
    $db_username = 'username';
    $db_password = 'password';
    $db_name = 'gestione_supermercati';
?>
</pre>
Il file con le credenziali non &egrave; stato inserito nella repository per motivi di sicurezza e convenienza.
<br>
Dopo questi passaggi, il codice potr&agrave; essere eseguito correttamente.
<br>
<br>
<br>
<br>
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
</a>
<br />
This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

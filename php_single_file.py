#! D:\Programmi\python-3.9.5-embed-amd64\python.exe

in_files = [
    "admin.php",
    "carrello.php",
    "connessioneDB.php",
    "dipendente.php",
    "index.php",
    "login.php",
    "logout.php",
    "ordina.php",
    "prodotti.php",
    "prodotto.php",
    "punti_vendita.php",
    "registrati.php",
    "util.php",
    "credenziali_db.php"
]
out_file = "tutto.txt"

def main():
    out_f = open(out_file, 'w')
    for in_file in in_files:
        file = open("Codice\\php\\" + in_file, 'r')
        name = "\n<!-- file: " + in_file + " -->\n"
        print(name)
        file_content = file.read()
        out_f.write(name + file_content)
        file.close()
    out_f.close()
main()